<?php
declare(strict_types=1);

namespace App\Tests\Unit\Advert\Entity;


use App\Advert\Entity\Attribute;
use PHPUnit\Framework\TestCase;

class AttributeTest extends TestCase
{

    public function testCreate(): void
    {
        $attribute = new Attribute($type = Attribute::TYPE_INTEGER, false);

        parent::assertTrue($attribute->isInteger());
        parent::assertFalse($attribute->isRequired());
    }

    public function testTypeException(): void
    {
        $type = "123123";

        parent::expectException(\InvalidArgumentException::class);
        parent::expectExceptionMessage("Attribute type ({$type}) not found");

        new Attribute($type, false);
    }

    public function testVariants(): void
    {
        $variants = [
            'good',
            'bad',
            'old'
        ];

        $attribute = new Attribute($type = Attribute::TYPE_SELECT, true);

        $attribute->setVariants($variants);

        parent::assertEquals($variants, $attribute->getVariants());
        parent::assertTrue($attribute->isSelect());
        parent::assertTrue($attribute->isRequired());

    }

}