<?php
declare(strict_types=1);

namespace App\Tests\Unit\Auth;


use App\Auth\TokenEncoder;
use App\Auth\TokenHeaders;
use App\Users\Entity\User\Email;
use App\Users\Entity\User\Password;
use App\Users\Entity\User\Phone;
use App\Users\Entity\User\User;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

class TokenEncoderTest extends TestCase
{

    /**
     * @var EncoderInterface
     */
    private $encoder;

    public function setUp(): void
    {
        $this->encoder = new TokenEncoder(
            new TokenHeaders([
                'alg' => 'sha256',
                'type' => 'jwt'
            ]),
            "dawdkhagwdiahwd87y8dawd"
        );

    }

    public function testEncode(): void
    {
        $user = new User(
            $email = new Email("some@gmail.com"),
            $phone = new Phone("+7 (999) 999 99-99"),
            new Password($password = "blablabla222"));


        $token = $this->encoder->encode($user);

        echo "Token: " . $token;
        parent::assertRegExp("#^[a-z0-9\=]+\.[a-z0-9\=]+\.[a-z0-9\=]+$#i", $token);
    }


}