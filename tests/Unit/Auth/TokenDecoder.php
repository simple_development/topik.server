<?php
declare(strict_types=1);

namespace App\Tests\Unit\Auth;


use App\Auth\TokenDecoderInterface;
use App\Auth\TokenEncoder;
use App\Auth\TokenEncoderInterface;
use App\Auth\TokenHeaders;
use App\Users\Entity\User\Email;
use App\Users\Entity\User\Password;
use App\Users\Entity\User\Phone;
use App\Users\Entity\User\User;
use PHPUnit\Framework\TestCase;

class TokenDecoder extends TestCase
{

    /**
     * @var TokenDecoderInterface
     */
    private $decoder;

    /**
     * @var TokenEncoderInterface
     */
    private $encoder;

    /**
     * @throws \ReflectionException
     */
    public function setUp(): void
    {
        $this->decoder = new \App\Auth\TokenDecoder();
        $this->encoder = new TokenEncoder(
            new TokenHeaders([
                'alg' => 'sha256',
                'type' => 'jwt'
            ]),
            "dawdkhagwdiahwd87y8dawd"
        );
    }



    public function testDecode(): void
    {
        $user = new User(
            $email = new Email("some@gmail.com"),
            $phone = new Phone("+7 (999) 999 99-99"),
            new Password($password = "blablabla222"));


        $token = $this->encoder->encode($user);


        $accessToken = $this->decoder->decode($token);

        parent::assertEquals($user, $accessToken->getData());
        parent::assertEquals($email, $accessToken->getData()->getEmail());
        parent::assertEquals($phone, $accessToken->getData()->getPhone());
        parent::assertTrue($accessToken->getData()->verifyPassword($password));

    }

}