<?php
declare(strict_types=1);

namespace App\Tests\Unit\Users\Enitity\User;


use App\Users\Entity\User\Email;
use App\Users\Entity\User\Password;
use App\Users\Entity\User\Phone;
use App\Users\Entity\User\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    public function testCreate(): void
    {
        $user = new User(
            $email = new Email("some@gmail.com"),
            $phone = new Phone("+7 (999) 999 99-99"),
            new Password($password = "blablabla222"));

        parent::assertEquals($email, $user->getEmail());
        parent::assertEquals($phone, $user->getPhone());
        parent::assertTrue($user->verifyPassword($password));
    }


    public function testNotVerifyPassword(): void
    {
        $user = new User(
            $email = new Email("some@gmail.com"),
            $phone = new Phone("+7 (999) 999 99-99"),
            new Password($password = "blablabla222"));

        parent::assertNotTrue($user->verifyPassword("d21dwad"));
    }

}