<?php


use App\Controller\Advert\CategoryController as AdvertCategoryController;
use App\Controller\Files\FilesController;
use App\Controller\Firm\CategoryController;
use App\Controller\Firm\FirmController;
use App\Controller\Firm\SubCategoryController;
use App\Controller\GeoLocation\CityController;
use App\Controller\GeoLocation\CountryController;
use App\Controller\Users\UsersController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Loader\Configurator\CollectionConfigurator;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

/**
 * @param RoutingConfigurator $routes
 */
return function (RoutingConfigurator $routes): void
{
    /** @var CollectionConfigurator $api */
    $api = $routes
        ->collection('api.')
        ->prefix('/api')
        ->options([
                'utf-8' => true
        ]);

    ### category

    $api
        ->add("category.create", "/category/create")
        ->controller([CategoryController::class, "create"])
        ->methods([Request::METHOD_POST]);

    $api
        ->add("category.get.all", "/category/getAll")
        ->controller([CategoryController::class, "getAll"])
        ->methods([Request::METHOD_GET]);

    ### sub category

    $api
        ->add("sub.category.create", "/sub/category/create")
        ->controller([SubCategoryController::class, "create"])
        ->methods([Request::METHOD_POST]);

    $api
        ->add("sub.category.get.all", "/sub/category/getAll/{category_id}")
        ->controller([SubCategoryController::class, "getAll"])
        ->methods([Request::METHOD_GET])
        ->requirements([
            'category_id' => "\d+"
        ]);


    ### firm

    $api
        ->add("firm.get.all", "/firm/getAll")
        ->controller([FirmController::class, "getAll"])
        ->methods([Request::METHOD_GET]);

    $api
        ->add("firm.search", "/firm/search")
        ->controller([FirmController::class, "search"])
        ->methods([Request::METHOD_GET]);

    $api
        ->add("firm.create", "/firm/create")
        ->controller([FirmController::class, "create"])
        ->methods([Request::METHOD_POST]);

    $api
        ->add("firm.add.tags", "/firm/{firm_id}/tags")
        ->requirements(['firm_id' => "\d+"])
        ->controller([FirmController::class, "addTags"])
        ->methods([Request::METHOD_PATCH]);


    ### Country

    $api
        ->add("country.get.all", "/country/getAll")
        ->controller([CountryController::class, "getAll"])
        ->methods([Request::METHOD_GET]);

    $api
        ->add("country.create", "/country/create")
        ->controller([CountryController::class, "create"])
        ->methods([Request::METHOD_POST]);

    ### City

    $api
        ->add("city.get.all", "/city/getAll")
        ->controller([CityController::class, "getAll"])
        ->methods([Request::METHOD_GET]);

    $api
        ->add("city.create", "/city/create")
        ->controller([CityController::class, "create"])
        ->methods([Request::METHOD_POST]);


    ### Advert Category

    $api
        ->add("advert.category.show", "/advert/category/{id}/show")
        ->controller([AdvertCategoryController::class, "show"])
        ->requirements(['id' => "\d+"])
        ->methods([Request::METHOD_GET]);

    $api
        ->add("advert.category.list", "/advert/category/list")
        ->controller([AdvertCategoryController::class, "getCategories"])
        ->methods([Request::METHOD_GET]);

    $api
        ->add("advert.category.create", "/advert/category/create")
        ->controller([AdvertCategoryController::class, "create"])
        ->methods([Request::METHOD_POST]);

    $api
        ->add("advert.category.remove", "/advert/category/{id}/remove")
        ->controller([AdvertCategoryController::class, "remove"])
        ->requirements(['id' => "\d+"])
        ->methods([Request::METHOD_DELETE]);


    ### Auth

    $api
        ->add("auth.sign.up", "/auth/signup")
        ->controller([UsersController::class, "signUp"])
        ->methods([Request::METHOD_POST]);

    $api
        ->add("auth.login", "/auth/login")
        ->controller([UsersController::class, "login"])
        ->methods([Request::METHOD_POST]);


    $api
        ->add("auth.info", "/auth/info")
        ->controller([UsersController::class, "getInfo"])
        ->methods([Request::METHOD_GET]);


    ### Files

    $api
        ->add("files.upload", "/files/upload")
        ->controller([FilesController::class, "upload"])
        ->methods([Request::METHOD_POST]);


    $api
        ->add("files.preview", "/files/{hash}")
        ->controller([FilesController::class, "preview"])
        ->requirements(["hash" => "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?.*$"])
        ->methods([Request::METHOD_GET]);


    $api
        ->add("files.download", "/files/{hash}/download")
        ->controller([FilesController::class, "download"])
        ->requirements(["hash" => "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?.*$"])
        ->methods([Request::METHOD_GET]);
};