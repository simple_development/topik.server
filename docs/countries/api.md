**API для сервисов стран и городов**  

* protocol: **HTTP/HTTPS**
* Content-Type: **application/json**

---  

**Country Service**
--

**POST:**  _/api/country/create_

**Request:** ( object )  

``` 
{
  "name": string(255),
  "coords": Coords
}
```

**Response:** ( Country )

```
{
  "id": int,
  "name": string,
  "coords": Coords
}
```

---

**GET:** /api/country/getAll  


**Request:** ( query parameters ) 

```
no parameters
```

**Response:** ( Country[] )  
```
[
    {
        "id": int
        "name": string,
        "coords": Coords
    }
]
```


**City Service**
--

**POST:**  _/api/city/create_

**Request:** ( object )  

``` 
{
  "name": string(255),
  "coords": Coords,
  "country_id": int
}
```

**Response:** ( City )

```
{
  "id": int,
  "name": string,
  "coords": Coords,
  "country": Country
}
```
---

**GET:** /api/city/getAll  


**Request:** ( query parameters ) 

```
country_id: int
order: DESC | ASC
```

**Response:** ( City[] )  
```
[
    {
        "id": int
        "name": string,
        "coords": Coords,
        "country": Country
    }
]
```