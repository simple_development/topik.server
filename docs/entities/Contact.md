```java
    @var string|null
    private $mobile;

    @var string|null
    private $skype;

    @var string|null
    private $site;

    @var string|null
    private $email;

    @var string|null
    private $viber;

    @var string|null
    private $telegram;

    @var string|null
    private $instagram;

    @var string|null
    private $whatsUp;
```