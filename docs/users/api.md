**API для пользователя**  

* protocol: **HTTP/HTTPS**
* Content-Type: **application/json**

---  

* С каждым запросом, необходимо передавать заголовок **Authorization** в котором указывать access_token.   
Так сервер будет идентифицировать пользователя.  

**Users Service**
--

**POST:**  _/api/auth/signup_

**Request:** ( object )  

``` 
{
  "phone": format("+# (###) ### ##-##"),
  "email": string(255),
  "password": string(3...30)
}
```

**Response:** ( object )

```
{
  "access_token": string,
  "user_info": User
}
```

---

**GET:** /api/auth/info  

**Request:** ( headers ) 

```
Authorization: string
```

**Response:** ( User )  

___


**SubCategory Service**
--

**POST:**  _/api/auth/login_

**Request:** ( object )  

``` 
{
  "email": string,
  "password": string
}
```

**Response:** ( object )

```
{
    "access_token": string,
    "user_info": string
}
```