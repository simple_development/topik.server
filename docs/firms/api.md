**API для сервисов фирм**  

* protocol: **HTTP/HTTPS**
* Content-Type: **application/json**

---  

**Category Service**
--

**POST:**  _/api/category/create_

**Request:** ( object )  

``` 
{
  "name": string(255)
}
```

**Response:** ( object )

```
{
  "id": int,
  "name": string
}
```

---

**GET:** /api/category/getAll  

**Request:** ( query parameters ) 

```
limit: int (default)10  
offset: int (default)0
```

**Response:** ( Category[] )  
```
[
    {
        "id": int,
        "name": string,
        "subCategories": SubCategory[]
    }
]
```

___


**SubCategory Service**
--

**POST:**  _/api/sub/category/create_

**Request:** ( object )  

``` 
{
  "category_id": int,
  "name": string(255)
}
```

**Response:** ( object )

```
{
  "id": int,
  "name": string,
  "category": Category
}
```

---


**GET:** /api/sub/category/getAll/{category_id}  

**Requirements:**  
```
category_id: int
```

**Request:** ( query parameters ) 

```
limit: int (default)10  
offset: int (default)0
```

**Response:** ( SubCategory[] )  
```
[
    {
        "id": int,
        "name": string,
        "category": Category
    }
]
```


**Firm Service**
--

**POST:**  _/api/firm/create_

**Request:** ( object )  

``` 
{
  "name": string(255),
  "subCategory": sub_category_id,
  "contact": Contact,
  "schedule": Schedule,
  "city": city_id,
  "owners": user_id[]
}
```

**Response:** ( object )

```
{
  "id": int
  "name": string,
  "subCategory": SubCategory,
  "contact": Contact,
  "schedule": Schedule,
  "vip" : Enum(0 - default, 1 - vip, premuim - 2),
  "city": City,
  "tags": Tag[]
}
```

**POST:**  _/api/firm/{firm_id}/tags_

**Requirements:**  
```
firm_id: int
```

**Request:** ( object )  

``` 
{
  "tags": Tag[]
}
```

**Response:** ( object )

```
{
  "id": int
  "name": string,
  "subCategory": SubCategory,
  "contact": Contact,
  "schedule": Schedule,
  "vip" : Enum(0 - default, 1 - vip),
  "tags": Tag[]
}
```


---


**GET:** /api/firm/getAll  


**Request:** ( query parameters ) 

```
subcategory: int
limit: int (default)10  
offset: int (default)0
```

**Response:** ( Firm[] )  
```
[
    {
        "id": int
        "name": string,
        "subCategory": SubCategory,
        "contact": Contact,
        "schedule": Schedule,
        "vip" : Enum(0 - default, 1 - vip),
        "tags": Tag[]
    }
]
```

---


**GET:** /api/firm/search  


**Request:** ( query parameters ) 

*q is param query tags. Tags in lower case, each tag must be comma (",") separated

```
q: string
limit: int (default)10  
offset: int (default) 0
```

**Response:** ( Firm[] )  
```
[
    {
        "id": int
        "name": string,
        "subCategory": SubCategory,
        "contact": Contact,
        "schedule": Schedule,
        "vip" : Enum(0 - default, 1 - vip),
        "tags": Tag[]
    }
]
```