<?php
declare(strict_types=1);

namespace App\Controller\Files;


use App\Files\Service\FilesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class FilesController
 * @package App\Controller\Files
 */
class FilesController extends AbstractController
{
    /**
     * @var FilesService
     */
    private $filesService;

    public function __construct(FilesService $filesService)
    {
        $this->filesService = $filesService;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function upload(Request $request): JsonResponse
    {
        /** @var UploadedFile $file */
        $uploadedFile = $request->files->get('file');

        if($uploadedFile === null){
            throw new \LogicException("Не передан файл");
        }

        $file = $this->filesService->upload($uploadedFile);

        return $this->json($file);
    }


    /**
     * @param string $hash
     * @return BinaryFileResponse
     */
    public function preview(string $hash): BinaryFileResponse
    {
        $file = $this->filesService->getFile($hash);

        return $this->file($file->fullPath(), null, ResponseHeaderBag::DISPOSITION_INLINE);
    }


    /**
     * @param string $hash
     * @return BinaryFileResponse
     */
    public function download(string $hash): BinaryFileResponse
    {
        $file = $this->filesService->getFile($hash);

        return $this->file($file->fullPath());
    }
}