<?php
declare(strict_types=1);

namespace App\Controller\Advert;


use App\Advert\Service\CategoryService;
use App\Advert\Service\DataTransfer\AdvertCategoryDataTransfer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryController extends AbstractController
{

    /**
     * @var CategoryService
     */
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }


    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function create(Request $request, ValidatorInterface $validator, SerializerInterface $serializer): JsonResponse
    {
        $dto = $serializer->deserialize($request->getContent(), AdvertCategoryDataTransfer::class, "json");

        $violations = $validator->validate($dto);
        if($violations->count() > 0){
            $resp = $serializer->serialize($violations, "json");
            return JsonResponse::fromJsonString($resp, 400);
        }

        $category = $this->categoryService->create($dto);

        return $this->json($category, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                 'root'
            ]
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function getCategories(): JsonResponse
    {
        $categories = $this->categoryService->getCategories();

        return $this->json($categories);
    }

    public function show(int $id): JsonResponse
    {
        $category = $this->categoryService->show($id);

        return $this->json($category, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'root'
            ]
        ]);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function remove(int $id): JsonResponse
    {
        $category = $this->categoryService->remove($id);

        return $this->json($category, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'root'
            ]
        ]);
    }

}