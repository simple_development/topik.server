<?php
declare(strict_types=1);

namespace App\Controller\Users;


use App\Auth\TokenDecoderInterface;
use App\Auth\TokenEncoderInterface;
use App\Users\Service\DataTransfer\UserDataTransfer;
use App\Users\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UsersController extends AbstractController
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @param TokenEncoderInterface $tokenEncoder
     * @return JsonResponse
     */
    public function signUp(
        Request $request,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        TokenEncoderInterface $tokenEncoder
    ): JsonResponse
    {
        $dto = $serializer->deserialize(
            $request->getContent(),
            UserDataTransfer::class,
            "json"
            );

        $violations = $validator->validate($dto);

        if($violations->count() > 0){
            $resp = $serializer->serialize($violations, "json");
            return JsonResponse::fromJsonString($resp, 400);
        }

        $user = $this->userService->signUp($dto);

        $token = $tokenEncoder->encode($user);

        return $this->json([
            'access_token' => $token,
            'user_info' => $user
        ]);

    }

    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param TokenEncoderInterface $tokenEncoder
     * @return JsonResponse
     */
    public function login(
        Request $request,
        SerializerInterface $serializer,
        TokenEncoderInterface $tokenEncoder
    ): JsonResponse
    {
        $dto = $serializer->deserialize(
            $request->getContent(),
            UserDataTransfer::class,
            "json"
        );

        $user = $this->userService->login($dto);


        $token = $tokenEncoder->encode($user);

        return $this->json([
            'access_token' => $token,
            'user_info' => $user
        ]);
    }

    /**
     * @param Request $request
     * @param TokenDecoderInterface $tokenDecoder
     * @return JsonResponse
     */
    public function getInfo(Request $request, TokenDecoderInterface $tokenDecoder): JsonResponse
    {
        $token = $request->headers->get('Authorization');

        $accessToken = $tokenDecoder->decode($token);

        return $this->json($accessToken->getData());

    }
}