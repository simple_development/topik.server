<?php
declare(strict_types=1);

namespace App\Controller\Firm;


use App\Firm\DataTransfer\FirmDataTransfer;
use App\Firm\DataTransfer\TagListDataTransfer;
use App\Firm\Service\FirmServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FirmController extends AbstractController
{

    /**
     * @var FirmServiceInterface
     */
    private $firmService;

    public function __construct(FirmServiceInterface $firmService)
    {
        $this->firmService = $firmService;
    }

    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function create(Request $request, ValidatorInterface $validator, SerializerInterface $serializer): JsonResponse
    {
        $firmDto = $serializer->deserialize($request->getContent(), FirmDataTransfer::class, "json");

        $violations = $validator->validate($firmDto);
        if($violations->count() > 0){
            $resp = $serializer->serialize($violations, "json");
            return JsonResponse::fromJsonString($resp, 400);
        }

        $firm = $this->firmService->create($firmDto);

        return $this->json($firm, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'firms', 'subCategories', 'city',
                '__initializer__',
                '__cloner__',
                '__isInitialized__'
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $limit = $request->query->has('limit') ? $request->query->get('limit') : 10;
        $offset = $request->query->has('offset') ? $request->query->get('offset') : 0;


        $criteria = [];

        $request->query->has('sub_category') ? $criteria['subCategory'] = $request->query->get('sub_category') : false;
        $request->query->has('sub_category') ? $criteria['city'] = $request->query->get('city_id') : false;



        $firms = $this->firmService->getAll($criteria,['vip' => 'DESC'], (int)$limit, (int)$offset);

        return $this->json($firms, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'firms', 'subCategories', 'city',
                '__initializer__',
                '__cloner__',
                '__isInitialized__'
            ]
        ]);
    }


    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param int $firm_id
     * @return JsonResponse
     */
    public function addTags(Request $request, SerializerInterface $serializer, int $firm_id): JsonResponse
    {
        $tagList = $serializer->deserialize($request->getContent(), TagListDataTransfer::class, "json");

        $firm = $this->firmService->addTags($firm_id, $tagList);

        return $this->json($firm, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'firms', 'subCategories',
                '__initializer__',
                '__cloner__',
                '__isInitialized__'
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request): JsonResponse
    {
        $query = $request->query->get('q');
        $limit =  $request->query->get('limit') ?? 10;
        $offset = $request->query->get('offset') ?? 0;

        $firms = $this->firmService->search($query, (int)$limit, (int)$offset);

        return $this->json($firms, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'firms', 'subCategories',
                '__initializer__',
                '__cloner__',
                '__isInitialized__'
            ]
        ]);

    }

}