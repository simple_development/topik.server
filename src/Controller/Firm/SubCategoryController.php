<?php
declare(strict_types=1);

namespace App\Controller\Firm;


use App\Firm\DataTransfer\SubCategoryDataTransfer;
use App\Firm\Service\SubCategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SubCategoryController extends AbstractController
{
    /**
     * @var SubCategoryService
     */
    private $subCategoryService;

    /**
     * SubCategoryController constructor.
     * @param SubCategoryService $subCategoryService
     */
    public function __construct(SubCategoryService $subCategoryService)
    {
        $this->subCategoryService = $subCategoryService;
    }


    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function create(Request $request, ValidatorInterface $validator, SerializerInterface $serializer): JsonResponse
    {
        $subDTO = $serializer->deserialize($request->getContent(), SubCategoryDataTransfer::class, "json");

        $violations = $validator->validate($subDTO);

        if($violations->count() > 0){
            $resp = $serializer->serialize($violations, "json");
            return JsonResponse::fromJsonString($resp, 400);
        }

        $subCategory = $this->subCategoryService->create($subDTO);

        return $this->json($subCategory, 200, [], ["ignored_attributes" => ['subCategories']]);

    }


    /**
     * @param Request $request
     * @param int $category_id
     * @return JsonResponse
     */
    public function getAll(Request $request, int $category_id): JsonResponse
    {
        $limit = $request->query->get('limit') ?? 10;
        $offset = $request->query->get('offset') ?? 0;

        $subCategories = $this->subCategoryService->getAllFromCategory($category_id, (int)$limit, (int)$offset);

        return $this->json($subCategories, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'firms', 'subCategories',
                '__initializer__',
                '__cloner__',
                '__isInitialized__'
            ]
        ]);
    }
}