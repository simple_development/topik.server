<?php
declare(strict_types=1);

namespace App\Controller\Firm;


use App\Firm\DataTransfer\CategoryDataTransfer;
use App\Firm\Service\CategoryServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryController extends AbstractController
{

    /**
     * @var CategoryServiceInterface
     */
    private $categoryService;


    /**
     * CategoryController constructor.
     * @param CategoryServiceInterface $categoryService
     */
    public function __construct(CategoryServiceInterface $categoryService)
    {
        $this->categoryService = $categoryService;
    }


    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function create(Request $request, ValidatorInterface $validator, SerializerInterface $serializer): JsonResponse
    {
        /** @var CategoryDataTransfer $categoryDTO */
        $categoryDTO = $serializer->deserialize(
            $request->getContent(), CategoryDataTransfer::class, 'json');

        $violations = $validator->validate($categoryDTO);

        if($violations->count() > 0){
            $resp = $serializer->serialize($violations, 'json');
            return JsonResponse::fromJsonString($resp, 400);
        }

        $category = $this->categoryService->create($categoryDTO);

        return $this->json($category);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $limit = $request->query->get('limit') ?? 10;
        $offset = $request->query->get('offset') ?? 0;


        $categories = $this->categoryService->getAll((int)$limit, (int)$offset);

        return $this->json($categories, 200, [], [
            'ignored_attributes' => ['firms', 'category']
        ]);
    }

}