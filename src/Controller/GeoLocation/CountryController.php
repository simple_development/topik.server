<?php
declare(strict_types=1);

namespace App\Controller\GeoLocation;


use App\GeoLocation\Service\Country\CountryServiceInterface;
use App\GeoLocation\Service\DataTransfer\CountryDataTransfer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CountryController
 * @package App\Controller\GeoLocation
 */
class CountryController extends AbstractController
{

    /**
     * @var CountryServiceInterface
     */
    private $countryService;

    /**
     * CountryController constructor.
     * @param CountryServiceInterface $countryService
     */
    public function __construct(CountryServiceInterface $countryService)
    {
        $this->countryService = $countryService;
    }

    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function create(Request $request, ValidatorInterface $validator, SerializerInterface $serializer): JsonResponse
    {
        /** @var CountryDataTransfer $countryDto */
        $countryDto = $serializer->deserialize(
            $request->getContent(),
            CountryDataTransfer::class,
            "json");

        $violations = $validator->validate($countryDto);

        if($violations->count() > 0){
            $resp = $serializer->serialize($violations, "json");
            return JsonResponse::fromJsonString($resp, 400);
        }

        $country = $this->countryService->create($countryDto);

        return $this->json($country, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'country'
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $order = [];
        $criteria = [];


        $countries = $this->countryService->getAll($criteria, $order);

        return $this->json($countries, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'cities'
            ],
        ]);
    }
}