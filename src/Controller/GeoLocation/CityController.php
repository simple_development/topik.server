<?php
declare(strict_types=1);

namespace App\Controller\GeoLocation;


use App\GeoLocation\Service\City\CityServiceInterface;
use App\GeoLocation\Service\DataTransfer\CityDataTransfer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CityController
 * @package App\Controller\GeoLocation
 */
class CityController extends AbstractController
{

    /**
     * @var CityServiceInterface
     */
    private $cityService;

    /**
     * CityController constructor.
     * @param CityServiceInterface $cityService
     */
    public function __construct(CityServiceInterface $cityService)
    {
        $this->cityService = $cityService;
    }

    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function create(Request $request, ValidatorInterface $validator, SerializerInterface $serializer): JsonResponse
    {
        /** @var CityDataTransfer $countryDto */
        $cityDto = $serializer->deserialize(
            $request->getContent(),
            CityDataTransfer::class,
            "json");

        $violations = $validator->validate($cityDto);

        if($violations->count() > 0){
            $resp = $serializer->serialize($violations, "json");
            return JsonResponse::fromJsonString($resp, 400);
        }

        $city = $this->cityService->create($cityDto);

        return $this->json($city, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'cities'
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {

        $criteria = [];
        $order = [];

        $request->query->has('country_id') ? $criteria['country'] = $request->query->get('country_id') : false;
        $request->query->has('order_by') ? $order[$request->query->get('order_by')] = $request->query->get('order'): false;

        $cities = $this->cityService->getAll($criteria, $order);

        return $this->json($cities, 200, [], [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'cities',
                '__initializer__',
                '__cloner__',
                '__isInitialized__'
            ]
        ]);
    }
}