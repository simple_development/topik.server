<?php
declare(strict_types=1);

namespace App\Firm\Entity\Schedule;


class Workday
{
    /**
     * @var string|null
     */
    private $start;

    /**
     * @var string|null
     */
    private $lunch;

    /**
     * @var string|null
     */
    private $end;

    /**
     * @var bool
     */
    private $dayOff;


    public function __construct(?string $start = null, ?string $end = null, ?string $lunch = null, bool $dayOff = false)
    {
        $this->lunch = $lunch;
        $this->start = $start;
        $this->end = $end;
        $this->dayOff = $dayOff;
    }


    /**
     * @return string|null
     */
    public function getStart(): ?string
    {
        return $this->start;
    }

    /**
     * @return string|null
     */
    public function getLunch(): ?string
    {
        return $this->lunch;
    }

    /**
     * @return string|null
     */
    public function getEnd(): ?string
    {
        return $this->end;
    }

    /**
     * @return bool
     */
    public function isDayOff(): bool
    {
        return $this->dayOff;
    }

}