<?php
declare(strict_types=1);

namespace App\Firm\Entity\Schedule;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Schedule
 * @package App\Firm\Entity\Schedule
 * @ORM\Embeddable()
 */
class Schedule
{
    /**
     * @ORM\Column(type="object")
     * @var Workday
     */
    private $monday;

    /**
     * @ORM\Column(type="object")
     * @var Workday
     */
    private $tuesday;

    /**
     * @ORM\Column(type="object")
     * @var Workday
     */
    private $wednesday;

    /**
     * @ORM\Column(type="object")
     * @var Workday
     */
    private $thursday;

    /**
     * @ORM\Column(type="object")
     * @var Workday
     */
    private $friday;

    /**
     * @ORM\Column(type="object")
     * @var Workday
     */
    private $saturday;

    /**
     * @ORM\Column(type="object")
     * @var Workday
     */
    private $sunday;



    public function __construct(Workday $monday, Workday $tuesday, Workday $wednesday,
        Workday $thursday, Workday $friday, Workday $saturday, Workday $sunday
    )
    {
        $this->monday = $monday;
        $this->tuesday = $tuesday;
        $this->wednesday = $wednesday;
        $this->thursday = $thursday;
        $this->friday = $friday;
        $this->saturday = $saturday;
        $this->sunday = $sunday;
    }

    /**
     * @return Workday|null
     */
    public function getMonday(): ?Workday
    {
        return $this->monday;
    }

    /**
     * @return Workday|null
     */
    public function getTuesday(): ?Workday
    {
        return $this->tuesday;
    }

    /**
     * @return Workday|null
     */
    public function getWednesday(): ?Workday
    {
        return $this->wednesday;
    }

    /**
     * @return Workday|null
     */
    public function getThursday(): ?Workday
    {
        return $this->thursday;
    }

    /**
     * @return Workday|null
     */
    public function getFriday(): ?Workday
    {
        return $this->friday;
    }

    /**
     * @return Workday|null
     */
    public function getSaturday(): ?Workday
    {
        return $this->saturday;
    }

    /**
     * @return Workday|null
     */
    public function getSunday(): ?Workday
    {
        return $this->sunday;
    }

}