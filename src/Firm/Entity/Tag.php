<?php
declare(strict_types=1);

namespace App\Firm\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Tag
 * @package App\Firm\Entity
 * @ORM\Entity()
 * @ORM\Table(name="tags")
 */
class Tag
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\JoinTable("tags_firms")
     * @ORM\JoinColumn(name="firms_id", referencedColumnName="id")
     * @ORM\ManyToMany(targetEntity="App\Firm\Entity\Firm", inversedBy="tags", fetch="EXTRA_LAZY")
     * @var ArrayCollection
     */
    private $firms;

    public function __construct(string $name)
    {
        $this->firms = new ArrayCollection();
        $this->name = strtolower($name);
    }

    public function addFirm(Firm $firm): void
    {
        if($this->firms->contains($firm)){
            return;
        }

        $this->firms->add($firm);
        $firm->addTag($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Firm[]
     */
    public function getFirms(): array
    {
        return $this->firms->toArray();
    }
}