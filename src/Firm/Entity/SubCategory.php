<?php
declare(strict_types=1);

namespace App\Firm\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity()
 * @ORM\Table(name="sub_categories")
 * Class SubCategory
 * @package App\Firm\Entity
 */
class SubCategory
{

    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var integer|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Firm\Entity\Firm", mappedBy="subCategory", fetch="EXTRA_LAZY")
     * @var ArrayCollection
     */
    private $firms;

    /**
     * @ORM\ManyToOne(targetEntity="App\Firm\Entity\Category", inversedBy="subCategories", fetch="EXTRA_LAZY")
     * @var Category
     */
    private $category;

    /**
     * SubCategory constructor.
     * @param string $name
     * @param Category $category
     */
    public function __construct(string $name, Category $category)
    {
        $this->name = $name;
        $this->category = $category;
        $this->firms = new ArrayCollection();
    }

    public function rename(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Firm[]
     */
    public function getFirms(): array
    {
        return $this->firms->toArray();
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}