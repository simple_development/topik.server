<?php
declare(strict_types=1);

namespace App\Firm\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Class Category
 * @package App\Firm\Entity
 * @ORM\Entity()
 * @ORM\Table(name="categories")
 */
class Category
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Firm\Entity\SubCategory", mappedBy="category", fetch="EXTRA_LAZY")
     * @var ArrayCollection
     */
    private $subCategories;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->subCategories = new ArrayCollection();
    }

    public function rename(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getSubCategories(): array
    {
        return $this->subCategories->toArray();
    }


}