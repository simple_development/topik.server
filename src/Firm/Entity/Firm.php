<?php
declare(strict_types=1);

namespace App\Firm\Entity;


use App\Firm\Entity\Schedule\Schedule;
use App\GeoLocation\Entity\City\City;
use App\Users\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="firms")
 * @ORM\Entity()
 * Class Firm
 * @package App\Firm\Entity
 */
class Firm
{
    public const DEFAULT = 0;
    public const VIP = 1;
    public const PREMIUM = 2;

    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Embedded(class="App\Firm\Entity\Contact", columnPrefix="contact_")
     * @var Contact
     */
    private $contact;

    /**
     * @ORM\ManyToMany(targetEntity="App\Firm\Entity\Tag", mappedBy="firms", fetch="EXTRA_LAZY", cascade={"persist"})
     * @var ArrayCollection
     */
    private $tags;

    /**
     *
     * @ORM\JoinColumn(name="sub_categories_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="App\Firm\Entity\SubCategory", inversedBy="firms")
     * @var SubCategory
     */
    private $subCategory;

    /**
     * @ORM\Embedded(class="App\Firm\Entity\Schedule\Schedule", columnPrefix="schedule_")
     * @var Schedule
     */
    private $schedule;

    /**
     * @ORM\ManyToOne(targetEntity="App\GeoLocation\Entity\City\City")
     * @ORM\JoinColumn(name="cities_id", referencedColumnName="id", onDelete="SET NULL")
     * @var City|null
     */
    private $city;


    /**
     * @ORM\Column(type="smallint", nullable=false)
     * @var int
     */
    private $status;


    /**
     * @ORM\ManyToMany(targetEntity="App\Users\Entity\User\User")
     * @ORM\JoinTable(name="firms_users",
     *          joinColumns={@ORM\JoinColumn(name="firms_id", referencedColumnName="id")},
     *          inverseJoinColumns={@ORM\JoinColumn(name="users_id", referencedColumnName="id")}
     *      )
     * @var ArrayCollection
     */
    private $owners;

    /**
     * Firm constructor.
     * @param string $name
     * @param Contact $contact
     * @param Schedule $schedule
     * @param City|null $city
     * @param SubCategory|null $subCategory
     */
    public function __construct(string $name, Contact $contact, Schedule $schedule, ?City $city = null, ?SubCategory $subCategory = null)
    {
        $this->name = $name;
        $this->contact = $contact;
        $this->schedule = $schedule;
        $this->city = $city;
        $this->tags = new ArrayCollection();
        $this->owners = new ArrayCollection();
        $this->status = self::DEFAULT;

        if($subCategory){
            $this->setSubCategory($subCategory);
        }



    }

    /**
     * @param User $user
     */
    public function addOwner(User $user): void
    {
        if($this->owners->contains($user)){
            throw new \LogicException("Пользователь уже имеет доступ к фирме");
        }

        $this->owners->add($user);
    }

    /**
     * @param City $city
     * @return Firm
     */
    public function setCity(City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @param Tag $tag
     */
    public function addTag(Tag $tag): void
    {
        if($this->tags->contains($tag)){
            return;
        }

        $this->tags->add($tag);
        $tag->addFirm($this);
    }

    /**
     * @param SubCategory $subCategory
     * @return Firm
     */
    public function setSubCategory(SubCategory $subCategory): self
    {
        $this->subCategory = $subCategory;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Contact
     */
    public function getContact(): Contact
    {
        return $this->contact;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags->toArray();
    }

    /**
     * @return Schedule
     */
    public function getSchedule(): Schedule
    {
        return $this->schedule;
    }

    /**
     * @return SubCategory
     */
    public function getSubCategory(): SubCategory
    {
        return $this->subCategory;
    }


    /**
     * @return City|null
     */
    public function getCity(): ?City
    {
        return $this->city;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return User[]
     */
    public function getOwners(): array
    {
        return $this->owners->toArray();
    }

}