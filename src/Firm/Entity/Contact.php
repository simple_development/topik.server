<?php
declare(strict_types=1);

namespace App\Firm\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Contact
 * @package App\Firm\Entity
 * @ORM\Embeddable()
 */
class Contact
{
    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $mobile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $skype;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $site;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $viber;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $telegram;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $instagram;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $whatsUp;

    /**
     * @return string|null
     */
    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    /**
     * @param string|null $mobile
     */
    public function setMobile(?string $mobile): void
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string|null
     */
    public function getSkype(): ?string
    {
        return $this->skype;
    }

    /**
     * @param string|null $skype
     */
    public function setSkype(?string $skype): void
    {
        $this->skype = $skype;
    }

    /**
     * @return string|null
     */
    public function getSite(): ?string
    {
        return $this->site;
    }

    /**
     * @param string|null $site
     */
    public function setSite(?string $site): void
    {
        $this->site = $site;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getViber(): ?string
    {
        return $this->viber;
    }

    /**
     * @param string|null $viber
     */
    public function setViber(?string $viber): void
    {
        $this->viber = $viber;
    }

    /**
     * @return string|null
     */
    public function getTelegram(): ?string
    {
        return $this->telegram;
    }

    /**
     * @param string|null $telegram
     */
    public function setTelegram(?string $telegram): void
    {
        $this->telegram = $telegram;
    }

    /**
     * @return string|null
     */
    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    /**
     * @param string|null $instagram
     */
    public function setInstagram(?string $instagram): void
    {
        $this->instagram = $instagram;
    }

    /**
     * @return string|null
     */
    public function getWhatsUp(): ?string
    {
        return $this->whatsUp;
    }

    /**
     * @param string|null $whatsUp
     */
    public function setWhatsUp(?string $whatsUp): void
    {
        $this->whatsUp = $whatsUp;
    }
}