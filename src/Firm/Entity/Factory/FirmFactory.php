<?php
declare(strict_types=1);

namespace App\Firm\Entity\Factory;


use App\Firm\DataTransfer\FirmDataTransfer;
use App\Firm\Entity\Firm;

class FirmFactory
{
    public static function createFromDataTransfer(FirmDataTransfer $dataTransfer): Firm
    {
        $contact = ContactFactory::createFromDataTransfer($dataTransfer->contact);
        $schedule = ScheduleFactory::scheduleFromDataTransfer($dataTransfer->schedule);

        $firm = new Firm($dataTransfer->name, $contact, $schedule);

        return $firm;
    }
}