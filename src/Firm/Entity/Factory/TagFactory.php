<?php
declare(strict_types=1);

namespace App\Firm\Entity\Factory;


use App\Firm\DataTransfer\TagDataTransfer;
use App\Firm\Entity\Tag;

class TagFactory
{
    public static function createFromDataTransfer(TagDataTransfer $dataTransfer): Tag
    {
        $tag = new Tag($dataTransfer->name);

        return $tag;
    }
}