<?php
declare(strict_types=1);

namespace App\Firm\Entity\Factory;


use App\Firm\DataTransfer\ContactDataTransfer;
use App\Firm\Entity\Contact;

class ContactFactory
{
    public static function createFromDataTransfer(ContactDataTransfer $dataTransfer): Contact
    {
        $contact = new Contact();
        $contact->setEmail($dataTransfer->email);
        $contact->setInstagram($dataTransfer->instagram);
        $contact->setMobile($dataTransfer->mobile);
        $contact->setSite($dataTransfer->site);
        $contact->setSkype($dataTransfer->skype);
        $contact->setTelegram($dataTransfer->telegram);
        $contact->setViber($dataTransfer->viber);
        $contact->setWhatsUp($dataTransfer->whatsUp);

        return $contact;
    }
}