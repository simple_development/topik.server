<?php
declare(strict_types=1);

namespace App\Firm\Entity\Factory;


use App\Firm\DataTransfer\ScheduleDataTransfer;
use App\Firm\DataTransfer\WorkDayDataTransfer;
use App\Firm\Entity\Schedule\Schedule;
use App\Firm\Entity\Schedule\Workday;

class ScheduleFactory
{
    public static function workdayFromDataTransfer(WorkDayDataTransfer $dataTransfer): Workday
    {
        return new Workday(
            $dataTransfer->start,
            $dataTransfer->end,
            $dataTransfer->lunch,
            $dataTransfer->dayOff
        );
    }

    public static function scheduleFromDataTransfer(ScheduleDataTransfer $dataTransfer): Schedule
    {

        $monday         = self::workdayFromDataTransfer($dataTransfer->monday);
        $tuesday        = self::workdayFromDataTransfer($dataTransfer->tuesday);
        $wednesday      = self::workdayFromDataTransfer($dataTransfer->wednesday);
        $thursday       = self::workdayFromDataTransfer($dataTransfer->thursday);
        $friday         = self::workdayFromDataTransfer($dataTransfer->friday);
        $saturday       = self::workdayFromDataTransfer($dataTransfer->saturday);
        $sunday         = self::workdayFromDataTransfer($dataTransfer->sunday);

        $schedule = new Schedule($monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday);

        return $schedule;
    }
}