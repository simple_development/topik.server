<?php
declare(strict_types=1);

namespace App\Firm\Entity\Factory;


use App\Firm\DataTransfer\MarkDataTransfer;
use App\Firm\Entity\Mark;

class MarkFactory
{
    public static function createFromDataTransfer(MarkDataTransfer $dataTransfer): Mark
    {
        $mark = new Mark($dataTransfer->lng, $dataTransfer->lat);

        return $mark;
    }
}