<?php
declare(strict_types=1);

namespace App\Firm\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Mark
 * @package App\Firm\Entity
 * @ORM\Embeddable()
 */
class Mark
{

    /**
     * @ORM\Column(type="float", nullable=false)
     * @var float
     */
    private $lng;

    /**
     * @ORM\Column(type="float", nullable=false)
     * @var float
     */
    private $lat;

    public function __construct(float $lng, float $lat)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }
}