<?php
declare(strict_types=1);

namespace App\Firm\Service;


use App\Firm\DataTransfer\FirmDataTransfer;
use App\Firm\DataTransfer\TagListDataTransfer;
use App\Firm\Entity\Firm;

interface FirmServiceInterface
{
    /**
     * @param FirmDataTransfer $firmDataTransfer
     * @return Firm
     */
    public function create(FirmDataTransfer $firmDataTransfer): Firm;

    /**
     * @param array $criteria
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getAll(array $criteria = [], array $order = [], int $limit = 10, int $offset = 0): array;

    /**
     * @param int $subCategoryId
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
    public function getBySubCategory(int $subCategoryId, int $limit = 10, int $offset = 0): array;

    /**
     * @param int $firm_id
     * @param TagListDataTransfer $tagList
     * @return Firm
     */
    public function addTags(int $firm_id, TagListDataTransfer $tagList): Firm;

    /**
     * @param string $query
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
    public function search(string $query, int $limit = 10, int $offset = 0): array;
}