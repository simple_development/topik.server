<?php
declare(strict_types=1);

namespace App\Firm\Service;


use App\Firm\DataTransfer\SubCategoryDataTransfer;
use App\Firm\Entity\SubCategory;
use App\Firm\Repository\CategoryRepositoryInterface;
use App\Firm\Repository\SubCategoryRepositoryInterface;

class SubCategoryService implements SubCategoryServiceInterface
{

    /**
     * @var SubCategoryRepositoryInterface
     */
    private $subCategoryRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(
        SubCategoryRepositoryInterface $subCategoryRepository,
        CategoryRepositoryInterface $categoryRepository
    )
    {
        $this->subCategoryRepository = $subCategoryRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param SubCategoryDataTransfer $subCategoryDataTransfer
     * @return SubCategory
     */
    public function create(SubCategoryDataTransfer $subCategoryDataTransfer): SubCategory
    {
        $category = $this->categoryRepository->one(['id' => $subCategoryDataTransfer->category_id]);

        $subCategory = new SubCategory($subCategoryDataTransfer->name, $category);

        $this->subCategoryRepository->save($subCategory);

        return $subCategory;
    }

    /**
     * @param int $id
     * @param string $name
     * @return SubCategory
     */
    public function rename(int $id, string $name): SubCategory
    {
        $subCategory = $this->subCategoryRepository->one(['id' => $id]);

        $subCategory->rename($name);

        $this->subCategoryRepository->update($subCategory);

        return $subCategory;
    }

    /**
     * @param int $id
     * @return SubCategory
     */
    public function delete(int $id): SubCategory
    {
        $subCategory = $this->subCategoryRepository->one(['id' => $id]);

        $this->subCategoryRepository->remove($subCategory);

        return $subCategory;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getAll(int $limit = 10, int $offset = 0): array
    {
        return $this->subCategoryRepository->getAll($limit,$offset);
    }

    /**
     * @param int $category_id
     * @param int $limit
     * @param int $offset
     * @return SubCategory[]
     */
    public function getAllFromCategory(int $category_id, int $limit = 10, int $offset = 0): array
    {
        return $this->subCategoryRepository->findBy(['category' => $category_id], [], $limit, $offset);
    }
}