<?php
declare(strict_types=1);

namespace App\Firm\Service;


use App\Firm\DataTransfer\CategoryDataTransfer;
use App\Firm\Entity\Category;

interface CategoryServiceInterface
{
    /**
     * @param CategoryDataTransfer $categoryDataTransfer
     * @return Category
     */
    public function create(CategoryDataTransfer $categoryDataTransfer) : Category;

    /**
     * @param int $id
     * @param string $name
     * @return Category
     */
    public function rename(int $id, string $name): Category;

    /**
     * @param int $id
     * @return Category
     */
    public function delete(int $id): Category;

    /**
     * @param int $limit
     * @param int $offset
     * @return Category[]
     */
    public function getAll(int $limit = 10, int $offset = 0): array;
}