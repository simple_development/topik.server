<?php
declare(strict_types=1);

namespace App\Firm\Service;


use App\Firm\DataTransfer\SubCategoryDataTransfer;
use App\Firm\Entity\SubCategory;

interface SubCategoryServiceInterface
{
    /**
     * @param SubCategoryDataTransfer $subCategoryDataTransfer
     * @return SubCategory
     */
    public function create(SubCategoryDataTransfer $subCategoryDataTransfer): SubCategory;

    /**
     * @param int $id
     * @param string $name
     * @return SubCategory
     */
    public function rename(int $id, string $name): SubCategory;

    /**
     * @param int $id
     * @return SubCategory
     */
    public function delete(int $id): SubCategory;

    /**
     * @param int $limit
     * @param int $offset
     * @return SubCategory[]
     */
    public function getAll(int $limit = 10, int $offset = 0): array;

    /**
     * @param int $category_id
     * @param int $limit
     * @param int $offset
     * @return SubCategory[]
     */
    public function getAllFromCategory(int $category_id, int $limit = 10, int $offset = 0): array;
}