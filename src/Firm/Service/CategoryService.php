<?php
declare(strict_types=1);

namespace App\Firm\Service;


use App\Firm\DataTransfer\CategoryDataTransfer;
use App\Firm\Entity\Category;
use App\Firm\Repository\CategoryRepositoryInterface;

/**
 * Class CategoryService
 * @package App\Firm\Service
 */
class CategoryService implements CategoryServiceInterface
{

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param CategoryDataTransfer $categoryDataTransfer
     * @return Category
     */
    public function create(CategoryDataTransfer $categoryDataTransfer): Category
    {
        $category = new Category($categoryDataTransfer->name);

        $this->categoryRepository->save($category);

        return $category;
    }

    /**
     * @param int $id
     * @param string $name
     * @return Category
     */
    public function rename(int $id, string $name): Category
    {
        $category = $this->categoryRepository->one(['id' => $id]);

        $category->rename($name);

        $this->categoryRepository->update($category);

        return $category;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function delete(int $id): Category
    {
        $category = $this->categoryRepository->one(['id' => $id]);

        $this->categoryRepository->remove($category);

        return $category;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Category[]
     */
    public function getAll(int $limit = 10, int $offset = 0): array
    {
        return $this->categoryRepository->getAll($limit, $offset);
    }
}