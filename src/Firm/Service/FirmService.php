<?php
declare(strict_types=1);

namespace App\Firm\Service;


use App\Firm\DataTransfer\FirmDataTransfer;
use App\Firm\DataTransfer\TagDataTransfer;
use App\Firm\DataTransfer\TagListDataTransfer;
use App\Firm\Entity\Factory\FirmFactory;
use App\Firm\Entity\Firm;
use App\Firm\Entity\Tag;
use App\Firm\Repository\FirmRepositoryInterface;
use App\Firm\Repository\SubCategoryRepositoryInterface;
use App\Firm\Repository\TagRepositoryInterface;
use App\GeoLocation\Repository\CityRepositoryInterface;
use App\Users\Entity\User\User;
use Doctrine\Common\Persistence\ObjectManager;

class FirmService implements FirmServiceInterface
{

    /**
     * @var FirmRepositoryInterface
     */
    private $firmRepository;
    /**
     * @var TagRepositoryInterface
     */
    private $tagRepository;
    /**
     * @var SubCategoryRepositoryInterface
     */
    private $subCategoryRepository;
    /**
     * @var CityRepositoryInterface
     */
    private $cityRepository;
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * FirmService constructor.
     * @param FirmRepositoryInterface $firmRepository
     * @param TagRepositoryInterface $tagRepository
     * @param SubCategoryRepositoryInterface $subCategoryRepository
     * @param CityRepositoryInterface $cityRepository
     * @param ObjectManager $manager
     */
    public function __construct(
        FirmRepositoryInterface $firmRepository,
        TagRepositoryInterface $tagRepository,
        SubCategoryRepositoryInterface $subCategoryRepository,
        CityRepositoryInterface $cityRepository,
        ObjectManager $manager
    )
    {
        $this->firmRepository = $firmRepository;
        $this->tagRepository = $tagRepository;
        $this->subCategoryRepository = $subCategoryRepository;
        $this->cityRepository = $cityRepository;
        $this->manager = $manager;
    }

    /**
     * @param FirmDataTransfer $firmDataTransfer
     * @return Firm
     */
    public function create(FirmDataTransfer $firmDataTransfer): Firm
    {
        $firm = FirmFactory::createFromDataTransfer($firmDataTransfer);

        $subCategory = $this->subCategoryRepository->one(['id' => $firmDataTransfer->subCategory]);
        $city = $this->cityRepository->getOne(['id' => $firmDataTransfer->city]);

        $firm->setSubCategory($subCategory);
        $firm->setCity($city);

        $firm->addTag(new Tag($firm->getName()));

        $users = $this->manager->getRepository(User::class)->findWhereIdIn($firmDataTransfer->owners);

        foreach ($users as $user){
            $firm->addOwner($user);
        }

        $this->firmRepository->save($firm);

        return $firm;
    }


    /**
     * @param array $criteria
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getAll(array $criteria = [], array $order = [],int $limit = 10, int $offset = 0): array
    {
        return $this->firmRepository->getAll($criteria, $order, $limit, $offset);
    }

    /**
     * @param int $firm_id
     * @param TagListDataTransfer $tagList
     * @return Firm
     */
    public function addTags(int $firm_id, TagListDataTransfer $tagList): Firm
    {
        $firm = $this->firmRepository->findOneById($firm_id);


        $tags = $this->tagRepository->allByName($tagList->getTagNames());

        foreach ($tags as $tag){
            $firm->addTag($tag);
        }

        /** @var TagDataTransfer[] $filteredTags */
        $filteredTags = array_filter($tagList->toArray(), function($tag) use ($tags): bool{
            /** @var TagDataTransfer $tag */

            foreach ($tags as $tagObject){
                if($tagObject->getName() == $tag->name){
                    return false;
                }
            }
            return true;
        });

        foreach ($filteredTags as $tagDataTransfer){
            $tag = new Tag($tagDataTransfer->name);

            $firm->addTag($tag);
        }



        $this->firmRepository->update($firm);

        return $firm;
    }

    /**
     * @param string $query
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
    public function search(string $query, int $limit = 10, int $offset = 0): array
    {
        /** @var string[] $tags */
        $tags = explode(",", strtolower(trim($query)));

        $firms = $this->firmRepository->findByTagNames($tags, $limit, $offset);

        return $firms;
    }

    /**
     * @param int $subCategoryId
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
    public function getBySubCategory(int $subCategoryId, int $limit = 10, int $offset = 0): array
    {
        $subCategory = $this->subCategoryRepository->one(['id' => $subCategoryId]);

        return $this->firmRepository->findBySubCategory($subCategory, $limit, $offset);
    }
}