<?php
declare(strict_types=1);

namespace App\Firm\Repository\Exception;


class NotFoundRepositoryException extends RepositoryException
{

}