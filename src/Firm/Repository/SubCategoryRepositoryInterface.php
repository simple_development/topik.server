<?php
declare(strict_types=1);

namespace App\Firm\Repository;


use App\Firm\Entity\SubCategory;
use App\Firm\Repository\Exception\NotFoundRepositoryException;

interface SubCategoryRepositoryInterface
{
    /**
     * @param SubCategory $subCategory
     * @return SubCategory
     */
    public function save(SubCategory $subCategory): SubCategory;

    /**
     * @param SubCategory $subCategory
     * @return SubCategory
     */
    public function update(SubCategory $subCategory): SubCategory;

    /**
     * @throws NotFoundRepositoryException
     * @param array $criteria
     * @param array|null $order
     * @return SubCategory
     */
    public function one(array $criteria, ?array $order = null): SubCategory;

    /**
     * @param SubCategory $subCategory
     * @return SubCategory
     */
    public function remove(SubCategory $subCategory): SubCategory;


    /**
     * @param int $limit
     * @param int $offset
     * @return SubCategory[]
     */
    public function getAll(int $limit, int $offset): array;


    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return SubCategory[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
}