<?php
declare(strict_types=1);

namespace App\Firm\Repository;


use App\Firm\Entity\Tag;
use App\Firm\Repository\Exception\NotFoundRepositoryException;

/**
 * Interface TagRepositoryInterface
 * @package App\Firm\Repository
 */
interface TagRepositoryInterface
{
    /**
     * @param Tag $tag
     * @return Tag
     */
    public function save(Tag $tag): Tag;

    /**
     * @param array $criteria
     * @param array|null $order
     * @throws NotFoundRepositoryException
     * @return Tag
     */
    public function one(array $criteria, ?array $order = null): Tag;

    /**
     * @param array $criteria
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return Tag[]
     */
    public function all(array $criteria, array $order, int $limit, int $offset): array;

    /**
     * @param Tag $tag
     * @return Tag
     */
    public function remove(Tag $tag): Tag;


    /**
     * @param array $names
     * @return Tag[]
     */
    public function allByName(array $names): array;
    /**
     * @param array $criteria
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return Tag[]
     */
    //public function allLike(array $criteria, array $order, int $limit, int $offset): array;
}