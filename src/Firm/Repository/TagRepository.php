<?php
declare(strict_types=1);

namespace App\Firm\Repository;


use App\Firm\Entity\Tag;
use App\Firm\Repository\Exception\NotFoundRepositoryException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

class TagRepository extends ServiceEntityRepository implements TagRepositoryInterface
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Tag::class);
        $this->manager = $manager;
    }

    /**
     * @param Tag $tag
     * @return Tag
     */
    public function save(Tag $tag): Tag
    {
        $this->manager->persist($tag);
        $this->manager->flush();

        return $tag;
    }

    /**
     * @param array $criteria
     * @param array|null $order
     * @throws NotFoundRepositoryException
     * @return Tag
     */
    public function one(array $criteria, ?array $order = null): Tag
    {
        /** @var Tag $tag */
        if(!$tag = parent::findOneBy($criteria, $order)){
            throw new NotFoundRepositoryException("Тег не найден");
        }

        return $tag;
    }

    /**
     * @param array $criteria
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return Tag[]
     */
    public function all(array $criteria, array $order, int $limit, int $offset): array
    {
        $tags = parent::findBy($criteria, $order, $limit, $offset);

        return $tags;
    }

    /**
     * @param Tag $tag
     * @return Tag
     */
    public function remove(Tag $tag): Tag
    {
        $this->manager->remove($tag);
        $this->manager->flush();

        return $tag;
    }

    /**
     * @param array $names
     * @return Tag[]
     */
    public function allByName(array $names): array
    {
        $qb = parent::createQueryBuilder('b');

        $query = $qb
                    ->select('t')
                    ->from(Tag::class, 't')
                    ->where($qb->expr()->in('t.name', $names))->getQuery();

        return $query->getResult();

    }
}