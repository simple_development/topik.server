<?php
declare(strict_types=1);

namespace App\Firm\Repository;


use App\Firm\Entity\Category;
use App\Firm\Repository\Exception\NotFoundRepositoryException;

interface CategoryRepositoryInterface
{
    /**
     * @param Category $category
     * @return Category
     */
    public function save(Category $category): Category;


    /**
     * @param array $criteria
     * @param array|null $order
     * @throws NotFoundRepositoryException
     * @return Category
     */
    public function one(array $criteria, ?array $order = null): Category;

    /**
     * @param Category $category
     * @return Category
     */
    public function remove(Category $category): Category;

    /**
     * @param Category $category
     * @return Category
     */
    public function update(Category $category): Category;

    /**
     * @param int $limit
     * @param int $offset
     * @return Category[]
     */
    public function getAll(int $limit, int $offset): array;
}