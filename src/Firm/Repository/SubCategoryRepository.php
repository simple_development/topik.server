<?php
declare(strict_types=1);

namespace App\Firm\Repository;


use App\Firm\Entity\SubCategory;
use App\Firm\Repository\Exception\NotFoundRepositoryException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

class SubCategoryRepository extends ServiceEntityRepository implements SubCategoryRepositoryInterface
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, SubCategory::class);
        $this->manager = $manager;
    }

    /**
     * @param SubCategory $subCategory
     * @return SubCategory
     */
    public function save(SubCategory $subCategory): SubCategory
    {
        $this->manager->persist($subCategory);
        $this->manager->flush();

        return $subCategory;
    }

    /**
     * @param SubCategory $subCategory
     * @return SubCategory
     */
    public function update(SubCategory $subCategory): SubCategory
    {
        $this->manager->flush();

        return $subCategory;
    }

    /**
     * @throws NotFoundRepositoryException
     * @param array $criteria
     * @param array|null $order
     * @return SubCategory
     */
    public function one(array $criteria, ?array $order = null): SubCategory
    {
        /** @var SubCategory $subCategory */
        if(!$subCategory = parent::findOneBy($criteria, $order)){
            throw new NotFoundRepositoryException("Под категория не найдена");
        }

        return $subCategory;
    }

    /**
     * @param SubCategory $subCategory
     * @return SubCategory
     */
    public function remove(SubCategory $subCategory): SubCategory
    {

        $this->manager->remove($subCategory);

        return $subCategory;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return SubCategory[]
     */
    public function getAll(int $limit, int $offset): array
    {
        return parent::findBy([],[], $limit, $offset);
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return parent::findBy($criteria, $orderBy, $limit, $offset); // TODO: Change the autogenerated stub
    }
}