<?php
declare(strict_types=1);

namespace App\Firm\Repository;


use App\Firm\Entity\Category;
use App\Firm\Repository\Exception\NotFoundRepositoryException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryRepository extends ServiceEntityRepository implements CategoryRepositoryInterface
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Category::class);
        $this->manager = $manager;
    }

    /**
     * @param Category $category
     * @return Category
     */
    public function save(Category $category): Category
    {
        $this->manager->persist($category);
        $this->manager->flush();

        return $category;
    }


    /**
     * @param array $criteria
     * @param array|null $order
     * @throws NotFoundRepositoryException
     * @return Category
     */
    public function one(array $criteria, ?array $order = null): Category
    {

        /** @var Category $category */
        if(!$category = parent::findOneBy($criteria, $order)){
            throw new NotFoundRepositoryException("Категория не найдена");
        }

        return $category;
    }

    /**
     * @param Category $category
     * @return Category
     */
    public function remove(Category $category): Category
    {
        $this->manager->remove($category);
        $this->manager->flush();

        return $category;
    }

    /**
     * @param Category $category
     * @return Category
     */
    public function update(Category $category): Category
    {
        $this->manager->flush();

        return $category;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Category[]
     */
    public function getAll(int $limit, int $offset): array
    {
        return parent::findBy([], [], $limit, $offset);
    }
}