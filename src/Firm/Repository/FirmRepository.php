<?php
declare(strict_types=1);

namespace App\Firm\Repository;


use App\Firm\Entity\Firm;
use App\Firm\Entity\SubCategory;
use App\Firm\Repository\Exception\NotFoundRepositoryException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

class FirmRepository extends ServiceEntityRepository implements FirmRepositoryInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Firm::class);
        $this->manager = $manager;
    }

    /**
     * @param Firm $firm
     * @return Firm
     */
    public function save(Firm $firm): Firm
    {
        $this->manager->persist($firm);
        $this->manager->flush();

        return $firm;
    }

    /**
     * @param array $criteria
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
    public function getAll(array $criteria, array $order,int $limit, int $offset): array
    {
        return parent::findBy($criteria, $order, $limit, $offset);
    }

    /**
     * @param int $id
     * @return Firm
     */
    public function findOneById(int $id): Firm
    {
        /** @var Firm $firm */
        if(!$firm = parent::findOneBy(['id' => $id])){
            throw new NotFoundRepositoryException("Фирма не найдена");
        }

        return $firm;
    }

    /**
     * @param Firm $firm
     * @return Firm
     */
    public function update(Firm $firm): Firm
    {
        $this->manager->flush();
        return $firm;
    }

    /**
     * @param string[] $tags
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
    public function findByTagNames(array $tags, int $limit, int $offset): array
    {
        $qb = parent::createQueryBuilder('b');

        $query = $qb->select('f')
            ->from(Firm::class, "f")
            ->innerJoin('f.tags', 't')
            ->where($qb->expr()->in('t.name', $tags))
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery();

        $r = $query->getResult();

        return $r;
    }


    /**
     * @param SubCategory $subCategory
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
    public function findBySubCategory(SubCategory $subCategory, int $limit, int $offset): array
    {
        $qb = parent::createQueryBuilder('b');

        $query = $qb->select('f')
            ->from(Firm::class, "f")
            ->where('f.subCategory = :subCategoryId')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->setParameter('subCategoryId', $subCategory->getId())
            ->getQuery();

        $r = $query->getResult();

        return $r;
    }
}