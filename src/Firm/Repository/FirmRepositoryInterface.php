<?php
declare(strict_types=1);

namespace App\Firm\Repository;


use App\Firm\Entity\Firm;
use App\Firm\Entity\SubCategory;

interface FirmRepositoryInterface
{
    /**
     * @param Firm $firm
     * @return Firm
     */
    public function save(Firm $firm): Firm;

    /**
     * @param Firm $firm
     * @return Firm
     */
    public function update(Firm $firm): Firm;

    /**
     * @param array $criteria
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
    public function getAll(array $criteria, array $order,int $limit, int $offset): array;

    /**
     * @param int $id
     * @return Firm
     */
    public function findOneById(int $id): Firm;

    /**
     * @param string[] $tags
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
    public function findByTagNames(array $tags, int $limit, int $offset): array;

    /**
     * @param SubCategory $subCategory
     * @param int $limit
     * @param int $offset
     * @return Firm[]
     */
     public function findBySubCategory(SubCategory $subCategory, int $limit, int $offset): array;
}