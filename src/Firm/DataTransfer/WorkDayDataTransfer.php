<?php
declare(strict_types=1);

namespace App\Firm\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class WorkDayDataTransfer
{
    /**
     * @Assert\Type(type="string")
     * @var string|null
     */
    public $start;

    /**
     * @Assert\Type(type="string")
     * @var string|null
     */
    public $lunch;

    /**
     * @Assert\Type(type="string")
     * @var string|null
     */
    public $end;

    /**
     * @Assert\Type(type="bool", message="Day off must be bool")
     * @var bool
     */
    public $dayOff;
}