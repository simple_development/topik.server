<?php
declare(strict_types=1);

namespace App\Firm\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class FirmDataTransfer
{

    /**
     * @Assert\Type(type="string", message="Firm name must be string")
     * @Assert\NotBlank(message="Укажите название фирмы")
     * @var string
     */
    public $name;

    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @var ContactDataTransfer
     */
    public $contact;

    /**
     * @var TagDataTransfer[]
     */
    public $tags = [];

    /**
     * @Assert\Type(type="int", message="Sub category must be integer")
     * @Assert\NotBlank(message="Укажите под категорию")
     * @var int
     */
    public $subCategory;

    /**
     * @Assert\Type(type="int", message="Sub category must be integer")
     * @Assert\NotBlank(message="Укажите город")
     * @var int
     */
    public $city;

    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @var ScheduleDataTransfer
     */
    public $schedule;

    /**
     * @var int
     */
    public $status;

    /**
     * @Assert\Type(type="array", message="owners must be array")
     * @Assert\NotBlank(message="Укажите владельцев фирмы")
     * @var int[]
     */
    public $owners = [];
}