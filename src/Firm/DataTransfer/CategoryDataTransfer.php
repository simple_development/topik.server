<?php
declare(strict_types=1);

namespace App\Firm\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;


class CategoryDataTransfer
{
    /**
     * @Assert\NotBlank(message="Укажите имя категории")
     * @var string
     */
    public $name;
}