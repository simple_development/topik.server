<?php
declare(strict_types=1);

namespace App\Firm\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class TagDataTransfer
{
    /**
     * @Assert\Type(type="string", message="tag name must be string")
     * @Assert\Length(min="2", minMessage="Название тега не менее 2 символов")
     * @var string
     */
    public $name;
}