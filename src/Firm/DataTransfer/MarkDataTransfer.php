<?php
declare(strict_types=1);

namespace App\Firm\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class MarkDataTransfer
{
    /**
     * @Assert\Type(type="float")
     * @var float
     */
    public $lng;

    /**
     * @Assert\Type(type="float")
     * @var float
     */
    public $lat;
}