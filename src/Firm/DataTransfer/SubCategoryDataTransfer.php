<?php
declare(strict_types=1);

namespace App\Firm\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class SubCategoryDataTransfer
{
    /**
     * @Assert\NotBlank(message="Укажите название под категории")
     * @var string
     */
    public $name;

    /**
     * @Assert\NotBlank(message="Укажите категорию")
     * @Assert\Type(type="int", message="Ошибка типа категории")
     * @var int
     */
    public $category_id;
}