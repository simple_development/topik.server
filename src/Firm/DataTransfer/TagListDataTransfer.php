<?php
declare(strict_types=1);

namespace App\Firm\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class TagListDataTransfer implements \ArrayAccess
{
    /**
     * @Assert\Valid()
     * @Assert\NotBlank(message="укажите теги")
     * @Assert\Type(type="array", message="Tags must be array of objects")
     * @var TagDataTransfer[]
     */
    public $tags = [];

    /**
     * @return string[]
     */
    public function getTagNames(): array
    {
        $names = [];
        foreach ($this->tags as $tag){
            $names[] = $tag->name;
        }

        return $names;
    }

    /**
     * @return TagDataTransfer[]
     */
    public function toArray(): array
    {
        return $this->tags;
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->tags[$offset]);
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return $this->tags[$offset];
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        if(!$value instanceof TagDataTransfer){
            throw new \LogicException("value must be instance of TagDataTransfer");
        }
        $this->tags[$offset] = $value;
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->tags[$offset]);
    }
}