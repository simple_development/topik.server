<?php
declare(strict_types=1);

namespace App\Firm\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class ScheduleDataTransfer
{
    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @var WorkDayDataTransfer
     */
    public $monday;

    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @var WorkDayDataTransfer
     */
    public $tuesday;

    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @var WorkDayDataTransfer
     */
    public $wednesday;

    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @var WorkDayDataTransfer
     */
    public $thursday;

    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @var WorkDayDataTransfer
     */
    public $friday;

    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @var WorkDayDataTransfer
     */
    public $saturday;

    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     * @var WorkDayDataTransfer
     */
    public $sunday;
}