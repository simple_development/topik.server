<?php
declare(strict_types=1);

namespace App\Firm\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class ContactDataTransfer
{
    /**
     * @Assert\Type(type="string|null")
     * @var string|null
     */
    public $mobile;

    /**
     * @Assert\Type(type="string|null")
     * @var string|null
     */
    public $skype;

    /**
     * @Assert\Type(type="string|null")
     * @var string|null
     */
    public $site;

    /**
     * @Assert\Type(type="string|null")
     * @var string|null
     */
    public $email;

    /**
     * @Assert\Type(type="string|null")
     * @var string|null
     */
    public $viber;

    /**
     * @Assert\Type(type="string|null")
     * @var string|null
     */
    public $telegram;

    /**
     * @Assert\Type(type="string|null")
     * @var string|null
     */
    public $instagram;

    /**
     * @Assert\Type(type="string|null")
     * @var string|null
     */
    public $whatsUp;
}