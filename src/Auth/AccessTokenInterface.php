<?php
declare(strict_types=1);

namespace App\Auth;


use App\Users\Entity\User\User;

interface AccessTokenInterface
{
    public function isVerify(): bool;

    public function getData(): User;

    public function getHeaders(): TokenHeadersInterface;

}