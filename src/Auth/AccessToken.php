<?php
declare(strict_types=1);

namespace App\Auth;


use App\Users\Entity\User\User;

class AccessToken implements AccessTokenInterface
{
    private $headers;
    private $data;
    private $sign;

    public function __construct(TokenHeadersInterface $headers, User $data, string $sign)
    {
        $this->headers = $headers;
        $this->data = $data;
        $this->sign = $sign;
    }


    public function isVerify(): bool
    {
        $s = base64_encode(
            hash($this->headers->getAlg(),
                serialize($this->headers) . serialize($this->data) . $_ENV['APP_SECRET']));

        return $s === $this->sign;
    }

    /**
     * @return TokenHeadersInterface
     */
    public function getHeaders(): TokenHeadersInterface
    {
        return $this->headers;
    }

    /**
     * @return User
     */
    public function getData(): User
    {
        return $this->data;
    }


}