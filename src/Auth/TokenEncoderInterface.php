<?php
declare(strict_types=1);

namespace App\Auth;


use App\Users\Entity\User\User;

interface TokenEncoderInterface
{
    public function encode(User $data): string;
}