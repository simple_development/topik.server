<?php
declare(strict_types=1);

namespace App\DataFixtures;


use App\Users\Entity\User\Email;
use App\Users\Entity\User\Password;
use App\Users\Entity\User\Phone;
use App\Users\Entity\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UsersFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for($i = 100; $i < 200; $i++){
            $user = new User(
                new Email("test.email{$i}@gmail.com"),
                new Phone("+7 ({$i}) {$i} 00-00"),
                new Password("123123")
            );

            $manager->persist($user);
        }

        $manager->flush();
    }
}