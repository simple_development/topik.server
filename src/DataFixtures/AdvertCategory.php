<?php
declare(strict_types=1);

namespace App\DataFixtures;


use App\Advert\Entity\Category\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AdvertCategory extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $transport = new Category("Транспорт");

        $auto = new Category("Авто");
        $auto->setParent($transport);

        $hummer = new Category("Внедорожники");
        $hummer->setParent($transport);

        $builds = new Category("Недвижимость");

        $new = new Category("Новостройки");
        $new->setParent($builds);

        $big = new Category("Катеджи");
        $big->setParent($builds);

        $elite = new Category("Элитные");
        $elite->setParent($big);

        $manager->persist($transport);
        $manager->persist($auto);
        $manager->persist($hummer);
        $manager->persist($builds);
        $manager->persist($new);
        $manager->persist($big);
        $manager->persist($elite);

        $manager->flush();

        echo "Categories initialized";


    }
}