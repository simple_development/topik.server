<?php
declare(strict_types=1);

namespace App\DataFixtures;


use App\Firm\DataTransfer\CategoryDataTransfer;
use App\Firm\DataTransfer\SubCategoryDataTransfer;
use App\Firm\Entity\Category;
use App\Firm\Service\CategoryServiceInterface;
use App\Firm\Service\SubCategoryServiceInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{

    /**
     * @var CategoryServiceInterface
     */
    private $categoryService;
    /**
     * @var SubCategoryServiceInterface
     */
    private $subCategoryService;

    public function __construct(CategoryServiceInterface $categoryService, SubCategoryServiceInterface $subCategoryService)
    {
        $this->categoryService = $categoryService;
        $this->subCategoryService = $subCategoryService;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < 10; $i++){
            $category = new CategoryDataTransfer();
            $category->name = "Category {$i}";

            $cat = $this->categoryService->create($category);


            echo "create category: {$cat->getId()}" . PHP_EOL;

            for($j = 0; $j < mt_rand(10, 20); $j++){
                $d = new SubCategoryDataTransfer();
                $d->name = "SubCategory {$j}";
                $d->category_id = $cat->getId();

                $s = $this->subCategoryService->create($d);

                echo "create sub category: {$s->getId()}" . PHP_EOL;
            }
        }
    }
}