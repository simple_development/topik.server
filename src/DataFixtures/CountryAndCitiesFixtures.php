<?php
declare(strict_types=1);

namespace App\DataFixtures;


use App\GeoLocation\Service\City\CityServiceInterface;
use App\GeoLocation\Service\Country\CountryServiceInterface;
use App\GeoLocation\Service\DataTransfer\CityDataTransfer;
use App\GeoLocation\Service\DataTransfer\CountryDataTransfer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use SebastianBergmann\CodeCoverage\Report\PHP;
use Symfony\Component\HttpKernel\KernelInterface;

class CountryAndCitiesFixtures extends Fixture
{

    /**
     * @var KernelInterface
     */
    private $kernel;
    /**
     * @var CountryServiceInterface
     */
    private $countryService;
    /**
     * @var CityServiceInterface
     */
    private $cityService;

    /**
     * CountryAndCitiesFixtures constructor.
     * @param KernelInterface $kernel
     * @param CityServiceInterface $cityService
     * @param CountryServiceInterface $countryService
     */
    public function __construct(KernelInterface $kernel, CityServiceInterface $cityService, CountryServiceInterface $countryService)
    {
        $this->kernel = $kernel;
        $this->countryService = $countryService;
        $this->cityService = $cityService;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $cities = json_decode(file_get_contents($this->kernel->getProjectDir() . "/countries/kyrgyzstan/cities.json"), true);

        $dto = new CountryDataTransfer();
        $dto->name = "Киргизия";



        $country = $this->countryService->create($dto);

        echo("Create country: {$country->getName()} with ID: {$country->getId()}" . PHP_EOL);


        foreach ($cities as $item){

            $dto = new CityDataTransfer();
            $dto->country_id = $country->getId();
            $dto->name = $item['city'];

            $city = $this->cityService->create($dto);

            echo("Create city: {$city->getName()} with ID: {$city->getId()}" . PHP_EOL);

        }

    }
}