<?php
declare(strict_types=1);

namespace App\Advert\Service;


use App\Advert\Entity\Category\Category;
use App\Advert\Service\DataTransfer\AdvertCategoryDataTransfer;
use App\Firm\Repository\Exception\RepositoryException;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryService
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * CategoryService constructor.
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param AdvertCategoryDataTransfer $dataTransfer
     * @return Category
     */
    public function create(AdvertCategoryDataTransfer $dataTransfer): Category
    {
        $repo = $this->em->getRepository(Category::class);

        $category = new Category($dataTransfer->title);

        if(is_int($dataTransfer->parent_id) && $parent = $repo->find($dataTransfer->parent_id)){
            $category->setParent($parent);
        }

        $this->em->persist($category);

        $this->em->flush();

        return $category;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $repo = $this->em->getRepository(Category::class);

        $categories = $repo->childrenHierarchy();

        return $categories;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function show(int $id): array
    {
        $repo = $this->em->getRepository(Category::class);

        if(!$object = $repo->find($id)){
            throw new RepositoryException("Категория не найдена");
        }

        $r = [$object];

        $r['__children'] = $repo->childrenHierarchy($object);

        return $r;
    }


    /**
     * @param int $id
     * @return Category
     */
    public function remove(int $id): Category
    {
        $repo = $this->em->getRepository(Category::class);

        if(!$category = $repo->find($id)){
            throw new RepositoryException("Категория не найдена");
        }

        $repo->removeFromTree($category);
        $this->em->clear();


        return $category;
    }
}