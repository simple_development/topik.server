<?php
declare(strict_types=1);

namespace App\Advert\Service\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class AdvertCategoryDataTransfer
{

    /**
     * @var int|null
     */
    public $parent_id;

    /**
     * @Assert\NotBlank(message="Укажите название категории")
     * @Assert\Type(type="string", message="category title must be string")
     * @var string
     */
    public $title;
}