<?php
declare(strict_types=1);

namespace App\Advert\Entity;

use App\Advert\Entity\Category\Category;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Attribute
 * @package App\Advert\Entity
 * @ORM\Table("advert_attributes")
 * @ORM\Entity()
 */
class Attribute
{
    public const TYPE_FLOAT = 'float';
    public const TYPE_INTEGER = 'integer';
    public const TYPE_SELECT = 'select';
    public const TYPE_STRING = 'string';


    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var integer
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=25)
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @var array|null
     */
    private $variants = null;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $required;

    /**
     * @ORM\ManyToOne(targetEntity="App\Advert\Entity\Category\Category", inversedBy="attributes")
     * @ORM\JoinColumn("advert_categories_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Category
     */
    private $category;

    public function __construct(string $type, bool $required)
    {
        $this->required = $required;
        $this->setType($type);
    }

    public function setVariants(array $variants): void
    {
        $this->variants = $variants;
        $this->type = self::TYPE_SELECT;
    }



    public function setType(string $type): void
    {
        if(!in_array($type, $this->typesList())){
            throw new \InvalidArgumentException("Attribute type ({$type}) not found");
        }

        $this->type = $type;
    }

    /**
     * @return array|null
     */
    public function getVariants(): ?array
    {
        return $this->variants;
    }

    public function typesList(): array
    {
        return [
            self::TYPE_STRING,
            self::TYPE_FLOAT,
            self::TYPE_INTEGER,
            self::TYPE_SELECT
        ];
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required === true;
    }

    /**
     * @return bool
     */
    public function isInteger(): bool
    {
        return $this->type === self::TYPE_INTEGER;
    }

    public function isFloat(): bool
    {
        return $this->type === self::TYPE_FLOAT;
    }

    /**
     * @return bool
     */
    public function isString(): bool
    {
        return $this->type === self::TYPE_STRING;
    }

    /**
     * @return bool
     */
    public function isSelect(): bool
    {
        return $this->type === self::TYPE_SELECT;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }
}