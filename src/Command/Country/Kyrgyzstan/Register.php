<?php
declare(strict_types=1);

namespace App\Command\Country\Kyrgyzstan;


use App\GeoLocation\Service\City\CityServiceInterface;
use App\GeoLocation\Service\Country\CountryServiceInterface;
use App\GeoLocation\Service\DataTransfer\CityDataTransfer;
use App\GeoLocation\Service\DataTransfer\CoordsDataTransfer;
use App\GeoLocation\Service\DataTransfer\CountryDataTransfer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class Register extends Command
{

    /**
     * @var string
     */
    protected static $defaultName = "app:cities:kyrgyzstan";
    /**
     * @var KernelInterface
     */
    private $kernel;
    /**
     * @var CountryServiceInterface
     */
    private $countryService;
    /**
     * @var CityServiceInterface
     */
    private $cityService;

    /**
     * Register constructor.
     * @param KernelInterface $kernel
     * @param CountryServiceInterface $countryService
     * @param CityServiceInterface $cityService
     */
    public function __construct(KernelInterface $kernel, CountryServiceInterface $countryService, CityServiceInterface $cityService)
    {
        parent::__construct();
        $this->kernel = $kernel;
        $this->countryService = $countryService;
        $this->cityService = $cityService;
    }


    protected function configure()
    {
        $this
            ->setDescription("register kyrgyzstan cities")
            ->setHelp("register kyrgyzstan cities from json ./countries/kyrgyzstan/cities.json");
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cities = json_decode(file_get_contents($this->kernel->getProjectDir() . "/countries/kyrgyzstan/cities.json"), true);

        $dto = new CountryDataTransfer();
        $dto->name = "Киргизия";

        $coords = new CoordsDataTransfer();
        $coords->lat = 41.2044;
        $coords->lng = 74.7661;

        $dto->coords = $coords;

        $country = $this->countryService->create($dto);

        $output->writeln("Create country: {$country->getName()} with ID: {$country->getId()}");


        foreach ($cities as $item){
            $coords = new CoordsDataTransfer();
            $coords->lng = (float)$item['lng'];
            $coords->lat = (float)$item['lat'];

            $dto = new CityDataTransfer();
            $dto->country_id = $country->getId();
            $dto->coords = $coords;
            $dto->name = $item['city'];

            $city = $this->cityService->create($dto);

            $output->writeln("Create city: {$city->getName()} with ID: {$city->getId()}");

        }

        $output->writeln("Success integrated Kyrgyzstan");

    }
}