<?php
declare(strict_types=1);

namespace App\EventListener;


use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class CorsListener
{
    public function onKernelResponse(FilterResponseEvent $event): void
    {
        $headers = $event->getResponse()->headers;

        $headers->set("Access-Control-Allow-Origin", "*");
        $headers->set("Access-Control-Allow-Methods"," GET, POST, PUT, PATCH, OPTIONS");
        $headers->set("Access-Control-Allow-Headers","Content-Type, Authorization");
    }
}