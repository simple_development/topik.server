<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190524201651 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE nested_categories DROP FOREIGN KEY FK_5663BBF9727ACA70');
        $this->addSql('ALTER TABLE nested_categories DROP FOREIGN KEY FK_5663BBF9A977936C');
        $this->addSql('CREATE TABLE advert_categories (id INT AUTO_INCREMENT NOT NULL, tree_root INT DEFAULT NULL, parent_id INT DEFAULT NULL, title VARCHAR(64) NOT NULL, lft INT NOT NULL, lvl INT NOT NULL, rgt INT NOT NULL, INDEX IDX_33DF3752A977936C (tree_root), INDEX IDX_33DF3752727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE advert_categories ADD CONSTRAINT FK_33DF3752A977936C FOREIGN KEY (tree_root) REFERENCES advert_categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE advert_categories ADD CONSTRAINT FK_33DF3752727ACA70 FOREIGN KEY (parent_id) REFERENCES advert_categories (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE nested_categories');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE advert_categories DROP FOREIGN KEY FK_33DF3752A977936C');
        $this->addSql('ALTER TABLE advert_categories DROP FOREIGN KEY FK_33DF3752727ACA70');
        $this->addSql('CREATE TABLE nested_categories (id INT AUTO_INCREMENT NOT NULL, tree_root INT DEFAULT NULL, parent_id INT DEFAULT NULL, title VARCHAR(64) NOT NULL COLLATE utf8mb4_unicode_ci, lft INT NOT NULL, lvl INT NOT NULL, rgt INT NOT NULL, INDEX IDX_5663BBF9A977936C (tree_root), INDEX IDX_5663BBF9727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE nested_categories ADD CONSTRAINT FK_5663BBF9727ACA70 FOREIGN KEY (parent_id) REFERENCES nested_categories (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE nested_categories ADD CONSTRAINT FK_5663BBF9A977936C FOREIGN KEY (tree_root) REFERENCES nested_categories (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE advert_categories');
    }
}
