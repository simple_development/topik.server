<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190527230231 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cities DROP coords_lng, DROP coords_lat');
        $this->addSql('ALTER TABLE countries DROP coords_lng, DROP coords_lat');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cities ADD coords_lng DOUBLE PRECISION NOT NULL, ADD coords_lat DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE countries ADD coords_lng DOUBLE PRECISION NOT NULL, ADD coords_lat DOUBLE PRECISION NOT NULL');
    }
}
