<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190520201559 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE firms (id INT AUTO_INCREMENT NOT NULL, sub_categories_id INT NOT NULL, name VARCHAR(255) NOT NULL, vip SMALLINT NOT NULL, contact_mobile VARCHAR(255) DEFAULT NULL, contact_skype VARCHAR(255) DEFAULT NULL, contact_site VARCHAR(255) DEFAULT NULL, contact_email VARCHAR(255) DEFAULT NULL, contact_viber VARCHAR(255) DEFAULT NULL, contact_telegram VARCHAR(255) DEFAULT NULL, contact_instagram VARCHAR(255) DEFAULT NULL, contact_whats_up VARCHAR(255) DEFAULT NULL, mark_lng DOUBLE PRECISION NOT NULL, mark_lat DOUBLE PRECISION NOT NULL, schedule_monday LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', schedule_tuesday LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', schedule_wednesday LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', schedule_thursday LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', schedule_friday LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', schedule_saturday LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', schedule_sunday LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', INDEX IDX_D854442B6DBFD369 (sub_categories_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sub_categories (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_1638D5A512469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags_firms (tag_id INT NOT NULL, firm_id INT NOT NULL, INDEX IDX_B330B273BAD26311 (tag_id), INDEX IDX_B330B27389AF7860 (firm_id), PRIMARY KEY(tag_id, firm_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE firms ADD CONSTRAINT FK_D854442B6DBFD369 FOREIGN KEY (sub_categories_id) REFERENCES sub_categories (id)');
        $this->addSql('ALTER TABLE sub_categories ADD CONSTRAINT FK_1638D5A512469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE tags_firms ADD CONSTRAINT FK_B330B273BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tags_firms ADD CONSTRAINT FK_B330B27389AF7860 FOREIGN KEY (firm_id) REFERENCES firms (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tags_firms DROP FOREIGN KEY FK_B330B27389AF7860');
        $this->addSql('ALTER TABLE firms DROP FOREIGN KEY FK_D854442B6DBFD369');
        $this->addSql('ALTER TABLE sub_categories DROP FOREIGN KEY FK_1638D5A512469DE2');
        $this->addSql('ALTER TABLE tags_firms DROP FOREIGN KEY FK_B330B273BAD26311');
        $this->addSql('DROP TABLE firms');
        $this->addSql('DROP TABLE sub_categories');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE tags_firms');
    }
}
