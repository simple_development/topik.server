<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190523000735 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE firms DROP FOREIGN KEY FK_D854442BCAC75398');
        $this->addSql('ALTER TABLE firms ADD CONSTRAINT FK_D854442BCAC75398 FOREIGN KEY (cities_id) REFERENCES cities (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE cities DROP FOREIGN KEY FK_D95DB16BAEBAE514');
        $this->addSql('ALTER TABLE cities ADD CONSTRAINT FK_D95DB16BAEBAE514 FOREIGN KEY (countries_id) REFERENCES countries (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cities DROP FOREIGN KEY FK_D95DB16BAEBAE514');
        $this->addSql('ALTER TABLE cities ADD CONSTRAINT FK_D95DB16BAEBAE514 FOREIGN KEY (countries_id) REFERENCES countries (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE firms DROP FOREIGN KEY FK_D854442BCAC75398');
        $this->addSql('ALTER TABLE firms ADD CONSTRAINT FK_D854442BCAC75398 FOREIGN KEY (cities_id) REFERENCES cities (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
