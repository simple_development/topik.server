<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190524140859 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE advert_categories (id INT AUTO_INCREMENT NOT NULL, tree_root INT DEFAULT NULL, parent_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, lft INT NOT NULL, rgt INT NOT NULL, lvl INT NOT NULL, INDEX IDX_33DF3752A977936C (tree_root), INDEX IDX_33DF3752727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE advert_categories ADD CONSTRAINT FK_33DF3752A977936C FOREIGN KEY (tree_root) REFERENCES advert_categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE advert_categories ADD CONSTRAINT FK_33DF3752727ACA70 FOREIGN KEY (parent_id) REFERENCES advert_categories (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE advert_categories DROP FOREIGN KEY FK_33DF3752A977936C');
        $this->addSql('ALTER TABLE advert_categories DROP FOREIGN KEY FK_33DF3752727ACA70');
        $this->addSql('DROP TABLE advert_categories');
    }
}
