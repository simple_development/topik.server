<?php
declare(strict_types=1);

namespace App\GeoLocation\Entity\City;


use App\GeoLocation\Entity\Country\Country;
use App\GeoLocation\Entity\Embeddable\Coords;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class City
 * @package App\GeoLocation\Entity\City
 * @ORM\Entity()
 * @ORM\Table(name="cities")
 */
class City
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;


    /**
     * @ORM\JoinColumn(name="countries_id", onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="App\GeoLocation\Entity\Country\Country", inversedBy="cities")
     * @var Country
     */
    private $country;


    /**
     * City constructor.
     * @param string $name
     * @param Country $country
     */
    public function __construct(string $name, Country $country)
    {
        $this->name = $name;
        $this->country = $country;
    }

    public function rename(string $name): void
    {
        $this->name = $name;
    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }
}