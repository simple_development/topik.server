<?php
declare(strict_types=1);

namespace App\GeoLocation\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Coords
 * @package App\GeoLocation\Entity\Embeddable
 * @ORM\Embeddable()
 */
class Coords
{
    /**
     * @ORM\Column(type="float", nullable=false)
     * @var float
     */
    private $lng;

    /**
     * @ORM\Column(type="float", nullable=false)
     * @var float
     */
    private $lat;

    /**
     * Coords constructor.
     * @param float $lng
     * @param float $lat
     */
    public function __construct(float $lng, float $lat)
    {
        $this->lng = $lng;
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }
}