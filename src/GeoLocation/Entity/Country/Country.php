<?php
declare(strict_types=1);

namespace App\GeoLocation\Entity\Country;

use App\GeoLocation\Entity\City\City;
use App\GeoLocation\Entity\Embeddable\Coords;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Country
 * @package App\GeoLocation\Entity\Country
 * @ORM\Entity()
 * @ORM\Table(name="countries")
 */
class Country
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="App\GeoLocation\Entity\City\City", orphanRemoval=true, mappedBy="country", fetch="EXTRA_LAZY")
     * @var ArrayCollection
     */
    private $cities;

    /**
     * Country constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->cities = new ArrayCollection();
    }

    /**
     * @param string $name
     */
    public function rename(string $name): void
    {
        $this->name = $name;
    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }



    /**
     * @return City[]
     */
    public function getCities(): array
    {
        return $this->cities->toArray();
    }


}