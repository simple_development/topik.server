<?php
declare(strict_types=1);

namespace App\GeoLocation\Repository;


use App\Firm\Repository\Exception\NotFoundRepositoryException;
use App\GeoLocation\Entity\Country\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

class CountryRepository extends ServiceEntityRepository implements CountryRepositoryInterface
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Country::class);
        $this->manager = $manager;
    }

    /**
     * @param array $criteria
     * @param array|null $order
     * @return Country
     */
    public function getOne(array $criteria, ?array $order = null): Country
    {
        /** @var Country $country */

        if(!$country = parent::findOneBy($criteria, $order)){
            throw new NotFoundRepositoryException("Страна не найдена");
        }

        return $country;
    }

    /**
     * @param Country $country
     * @return Country
     */
    public function save(Country $country): Country
    {
        $this->manager->persist($country);
        $this->manager->flush();

        return $country;
    }

    /**
     * @param Country $country
     * @return Country
     */
    public function update(Country $country): Country
    {
        $this->manager->flush();

        return $country;
    }

    /**
     * @param array $criteria
     * @param array $order
     * @return Country[]
     */
    public function getAll(array $criteria, array $order): array
    {
        return parent::findBy($criteria, $order);
    }
}