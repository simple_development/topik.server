<?php
declare(strict_types=1);

namespace App\GeoLocation\Repository;


use App\GeoLocation\Entity\Country\Country;

/**
 * Interface CountryRepositoryInterface
 * @package App\GeoLocation\Repository
 */
interface CountryRepositoryInterface
{
    /**
     * @param array $criteria
     * @param array $order
     * @return Country[]
     */
    public function getAll(array $criteria, array $order): array;

    /**
     * @param Country $country
     * @return Country
     */
    public function save(Country $country): Country;


    /**
     * @param array $criteria
     * @param array|null $order
     * @return Country
     */
    public function getOne(array $criteria, ?array $order = null): Country;


    /**
     * @param Country $country
     * @return Country
     */
    public function update(Country $country): Country;
}