<?php
declare(strict_types=1);

namespace App\GeoLocation\Repository;

use App\GeoLocation\Entity\City\City;

/**
 * Interface CityRepositoryInterface
 * @package App\GeoLocation\Repository
 */
interface CityRepositoryInterface
{
    /**
     * @param City $city
     * @return City
     */
    public function save(City $city): City;


    /**
     * @param array $criteria
     * @param array|null $order
     * @return City
     */
    public function getOne(array $criteria, ?array $order = null): City;

    /**
     * @param City $city
     * @return City
     */
    public function update(City $city): City;


    /**
     * @param array $criteria
     * @param array $order
     * @return City[]
     */
    public function getAll(array $criteria, array $order): array;
}