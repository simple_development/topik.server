<?php
declare(strict_types=1);

namespace App\GeoLocation\Repository;


use App\Firm\Repository\Exception\NotFoundRepositoryException;
use App\GeoLocation\Entity\City\City;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

class CityRepository extends ServiceEntityRepository implements CityRepositoryInterface
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, City::class);
        $this->manager = $manager;
    }

    /**
     * @param City $city
     * @return City
     */
    public function save(City $city): City
    {
        $this->manager->persist($city);
        $this->manager->flush();

        return $city;
    }

    /**
     * @param array $criteria
     * @param array|null $order
     * @return City
     */
    public function getOne(array $criteria, ?array $order = null): City
    {
        /** @var City $city */
        if(!$city = parent::findOneBy($criteria, $order)){
            throw new NotFoundRepositoryException("Город не найден");
        }

        return $city;
    }

    /**
     * @param City $city
     * @return City
     */
    public function update(City $city): City
    {
        $this->manager->flush();

        return $city;
    }

    /**
     * @param array $criteria
     * @param array $order
     * @return City[]
     */
    public function getAll(array $criteria, array $order): array
    {
        return parent::findBy($criteria, $order);
    }
}