<?php
declare(strict_types=1);

namespace App\GeoLocation\Service\City;


use App\GeoLocation\Entity\City\City;
use App\GeoLocation\Service\DataTransfer\CityDataTransfer;

interface CityServiceInterface
{
    /**
     * @param CityDataTransfer $dataTransfer
     * @return City
     */
    public function create(CityDataTransfer $dataTransfer): City;

    /**
     * @param int $id
     * @param CityDataTransfer $dataTransfer
     * @return City
     */
    public function edit(int $id, CityDataTransfer $dataTransfer): City;

    /**
     * @param array $criteria
     * @param array $order
     * @return City[]
     */
    public function getAll(array $criteria = [], array $order = []): array;
}