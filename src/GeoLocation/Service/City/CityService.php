<?php
declare(strict_types=1);

namespace App\GeoLocation\Service\City;


use App\GeoLocation\Entity\City\City;
use App\GeoLocation\Entity\Embeddable\Coords;
use App\GeoLocation\Repository\CityRepositoryInterface;
use App\GeoLocation\Repository\CountryRepositoryInterface;
use App\GeoLocation\Service\DataTransfer\CityDataTransfer;

class CityService implements CityServiceInterface
{

    /**
     * @var CityRepositoryInterface
     */
    private $cityRepository;
    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;

    public function __construct(CityRepositoryInterface $cityRepository, CountryRepositoryInterface $countryRepository)
    {
        $this->cityRepository = $cityRepository;
        $this->countryRepository = $countryRepository;
    }

    /**
     * @param CityDataTransfer $dataTransfer
     * @return City
     */
    public function create(CityDataTransfer $dataTransfer): City
    {
        $country = $this->countryRepository->getOne(['id' => $dataTransfer->country_id]);


        $city = new City($dataTransfer->name, $country);

        $this->cityRepository->save($city);

        return $city;
    }

    /**
     * @param int $id
     * @param CityDataTransfer $dataTransfer
     * @return City
     */
    public function edit(int $id, CityDataTransfer $dataTransfer): City
    {
        $city = $this->cityRepository->getOne(['id' => $id]);

        $city->rename($dataTransfer->name);

        $this->cityRepository->update($city);

        return $city;

    }

    /**
     * @param array $criteria
     * @param array $order
     * @return City[]
     */
    public function getAll(array $criteria = [], array $order = []): array
    {
        return $this->cityRepository->getAll($criteria, $order);
    }
}