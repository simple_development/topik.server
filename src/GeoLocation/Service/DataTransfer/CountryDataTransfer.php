<?php
declare(strict_types=1);

namespace App\GeoLocation\Service\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CountryDataTransfer
 * @package App\GeoLocation\Service\Country
 */
class CountryDataTransfer
{
    /**
     * @Assert\NotBlank(message="Укажите наименование страны")
     * @Assert\Type(type="string", message="Country name must be string")
     * @var string
     */
    public $name;

}