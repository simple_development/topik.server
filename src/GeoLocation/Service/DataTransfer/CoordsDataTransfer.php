<?php
declare(strict_types=1);

namespace App\GeoLocation\Service\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CoordsDataTransfer
 * @package App\GeoLocation\Service\DataTransfer
 */
class CoordsDataTransfer
{
    /**
     * @Assert\Type(type="float", message="coords lat must be float")
     * @Assert\NotBlank(message="Укажите шароту")
     * @var float
     */
    public $lat;

    /**
     * @Assert\Type(type="float", message="coords lat must be float")
     * @Assert\NotBlank(message="Укажите долготу")
     * @var float
     */
    public $lng;
}