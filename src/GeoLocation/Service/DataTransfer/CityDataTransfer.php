<?php
declare(strict_types=1);

namespace App\GeoLocation\Service\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CityDataTransfer
 * @package App\GeoLocation\Service\DataTransfer
 */
class CityDataTransfer
{
    /**
     * @Assert\NotBlank(message="Укажите наименование страны")
     * @Assert\Type(type="string", message="City name must be string")
     * @var string
     */
    public $name;


    /**
     * @Assert\NotBlank(message="Укажите категорию")
     * @Assert\Type(type="int", message="Country id must be int")
     * @var int
     */
    public $country_id;
}