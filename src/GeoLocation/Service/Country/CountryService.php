<?php
declare(strict_types=1);

namespace App\GeoLocation\Service\Country;


use App\GeoLocation\Entity\Country\Country;
use App\GeoLocation\Entity\Embeddable\Coords;
use App\GeoLocation\Repository\CountryRepositoryInterface;
use App\GeoLocation\Service\DataTransfer\CountryDataTransfer;

class CountryService implements CountryServiceInterface
{

    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;

    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @param CountryDataTransfer $dataTransfer
     * @return Country
     */
    public function create(CountryDataTransfer $dataTransfer): Country
    {
        $country = new Country($dataTransfer->name);

        $this->countryRepository->save($country);

        return $country;
    }

    /**
     * @param int $id
     * @param CountryDataTransfer $dataTransfer
     * @return Country
     */
    public function edit(int $id, CountryDataTransfer $dataTransfer): Country
    {
        $country = $this->countryRepository->getOne(['id' => $id]);

        $country->rename($dataTransfer->name);


        $this->countryRepository->update($country);

        return $country;
    }

    /**
     * @param array $criteria
     * @param array $order
     * @return Country[]
     */
    public function getAll(array $criteria = [], array $order = []): array
    {
        return $this->countryRepository->getAll($criteria, $order);
    }
}