<?php
declare(strict_types=1);

namespace App\GeoLocation\Service\Country;


use App\GeoLocation\Entity\Country\Country;
use App\GeoLocation\Service\DataTransfer\CountryDataTransfer;

/**
 * Interface CountryServiceInterface
 * @package App\GeoLocation\Service\Country
 */
interface CountryServiceInterface
{
    /**
     * @param CountryDataTransfer $dataTransfer
     * @return Country
     */
    public function create(CountryDataTransfer $dataTransfer): Country;

    /**
     * @param int $id
     * @param CountryDataTransfer $dataTransfer
     * @return Country
     */
    public function edit(int $id, CountryDataTransfer $dataTransfer): Country;

    /**
     * @param array $criteria
     * @param array $order
     * @return Country[]
     */
    public function getAll(array $criteria = [], array $order = []): array;
}