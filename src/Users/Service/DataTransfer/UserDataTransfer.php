<?php
declare(strict_types=1);

namespace App\Users\Service\DataTransfer;

use Symfony\Component\Validator\Constraints as Assert;

class UserDataTransfer
{

    /**
     * @Assert\Email(message="Не корректный email")
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $phone;
}