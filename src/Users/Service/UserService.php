<?php
declare(strict_types=1);

namespace App\Users\Service;


use App\Users\Entity\User\Email;
use App\Users\Entity\User\Password;
use App\Users\Entity\User\Phone;
use App\Users\Entity\User\User;
use App\Users\Service\DataTransfer\UserDataTransfer;
use Doctrine\Common\Persistence\ObjectManager;

class UserService
{
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }


    /**
     * @param UserDataTransfer $dataTransfer
     * @return User
     */
    public function signUp(UserDataTransfer $dataTransfer): User
    {
        $this->guardIsNewEmail($dataTransfer->email);

        $user = new User(
            new Email($dataTransfer->email),
            new Phone($dataTransfer->phone),
            new Password($dataTransfer->password)
        );

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    public function login(UserDataTransfer $dataTransfer): User
    {
        $repo = $this->em->getRepository(User::class);

        if(!$user = $repo->findOneBy(['email.value' => $dataTransfer->email])){
            throw new \LogicException("Пользователь с таким email не зарегистрирован");
        }

        return $user;
    }

    /**
     * @param string $email
     */
    private function guardIsNewEmail(string $email): void
    {
        $repo = $this->em->getRepository(User::class);

        if($repo->findOneBy(['email.value' => $email]) != null){
            throw new \LogicException("Данный email уже зарегистрирован");
        }
    }
}