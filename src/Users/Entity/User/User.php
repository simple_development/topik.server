<?php
declare(strict_types=1);

namespace App\Users\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package App\Users\Entity\User
 * @ORM\Entity(repositoryClass="App\Users\Repository\UsersRepository")
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Embedded(class="App\Users\Entity\User\Email", columnPrefix="email_")
     * @var Email
     */
    private $email;

    /**
     * @ORM\Embedded(class="App\Users\Entity\User\Phone", columnPrefix="phone_")
     * @var Phone
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=30)
     * @var string
     */
    private $password;


    public function __construct(Email $email, Phone $phone, Password $password)
    {
        $this->email = $email;
        $this->phone = $phone;
        $this->changePassword($password);
    }

    /**
     * @param Password $password
     */
    public function changePassword(Password $password): void
    {
        if($password->getLength() < 3){
            throw new \InvalidArgumentException("Пароль должен быть больше 3 символов");
        }
        if($password->getLength() >= 30){
            throw new \InvalidArgumentException("Пароль не более 30 символов");
        }
        $this->password = $password->getValue();
    }

    /**
     * @param Phone $phone
     */
    public function changePhone(Phone $phone): void
    {
        if($this->phone->equalTo($phone)){
            throw new \InvalidArgumentException("Этот номер уже используется");
        }

        $this->phone = $phone;
    }

    /**
     * @param string $password
     * @return bool
     */
    public function verifyPassword(string $password): bool
    {
        return $this->password == $password;
    }

    /**
     * @return Email
     */
    public function getEmail(): Email
    {
        return $this->email;
    }

    /**
     * @return Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

}
