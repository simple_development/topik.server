<?php
declare(strict_types=1);

namespace App\Users\Entity\User;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class Phone
 * @package App\Users\Entity\User
 * @ORM\Embeddable()
 */
class Phone
{
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $number;

    /**
     * Phone constructor.
     * @param string $number
     */
    public function __construct(string $number)
    {
        if(!preg_match("#\+\d\s\(\d{3}\)\s\d{3}\s\d{2}\-\d{2}#", $number)){
            throw new \InvalidArgumentException("Не корректный формат номера");
        }

        $this->number = $number;
    }

    /**
     * @param Phone $phone
     * @return bool
     */
    public function equalTo(Phone $phone): bool
    {
        return $this->getNumber() === $phone->getNumber();
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

}