<?php
declare(strict_types=1);

namespace App\Users\Entity\User;


class Password
{
    private $value;

    public function __construct(string $value)
    {
        if(!preg_match("#^[A-Z0-9\s]+$#i", $value)){
            throw new \InvalidArgumentException("Пароль может состоять только из латинских букв и цифр");
        }

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return strlen($this->getValue());
    }
}