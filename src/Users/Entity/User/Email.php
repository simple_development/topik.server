<?php
declare(strict_types=1);

namespace App\Users\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Email
 * @package App\Users\Entity\User
 * @ORM\Embeddable()
 */
class Email
{
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $value;


    /**
     * Email constructor.
     * @param string $email
     */
    public function __construct(string $email)
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            throw new \InvalidArgumentException("Не корректный email");
        }
        $this->value = $email;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }


}