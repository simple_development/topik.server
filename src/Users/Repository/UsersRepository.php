<?php
declare(strict_types=1);

namespace App\Users\Repository;


use App\Users\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class UsersRepository
 * @package App\Users\Repository
 * @method find($id, $lockMode = null, $lockVersion = null)
 * @method findOneBy(array $criteria, array $orderBy = null)
 * @method findAll()
 * @method findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, User::class);
    }


    /**
     * @param array $ids
     * @return User[]
     */
    public function findWhereIdIn(array $ids): array
    {
        $qb = parent::createQueryBuilder('b');

        $query = $qb->select('u')
            ->from(User::class, "u")
            ->where($qb->expr()->in('u.id', $ids))
            ->getQuery();

        $r = $query->getResult();

        return $r;
    }
}